package rotate.com.aakarshrestha.rotate.UI;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;
import rotate.com.aakarshrestha.rotate.Service.MyGCMRegistrationIntentService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Signup extends AppCompatActivity {

    protected EditText mUsername;
    protected EditText mEmailAddress;
    protected EditText mPassword;
    protected EditText mConfirmPassword;
    protected Button mSignupButton;
    protected ImageView mBackArrow;
    protected OkHttpClient client = new OkHttpClient();

    SharedPreferences mSharedPreferences;
    SharedPreferences.Editor mEditor;

    SharedPreferences mSharedPref;
    SharedPreferences.Editor mSEditor;

    private BroadcastReceiver mBroadcastReceiver;

    public ProgressBar signupProgressbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        /*
            setting background image
         */
        getWindow().setBackgroundDrawableResource(R.drawable.txtmebg);

        mBackArrow = (ImageView) findViewById(R.id.backArrowID);
        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Signup.this, MainActivity.class);
                startActivity(intent);

                /*
                Slide the activity
                */
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        signupProgressbar = (ProgressBar) findViewById(R.id.signupProgressbarID);
        mUsername = (EditText) findViewById(R.id.sUsernameID);
        mEmailAddress = (EditText) findViewById(R.id.sEmailID);
        mPassword = (EditText) findViewById(R.id.sPasswordID);
        mConfirmPassword = (EditText) findViewById(R.id.sConfirmPasswordID);
        mSignupButton = (Button) findViewById(R.id.signupButtonID);

        /*
            hide the progress bar
         */
        signupProgressbar.setVisibility(View.GONE);

        /*
            broadcastReceiver
         */
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //check type of intent filter
                if (intent.getAction().endsWith(MyGCMRegistrationIntentService.REGISTRATION_SUCCESS)) {
                    String token = intent.getStringExtra("token");

                    mSharedPref = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                    mSEditor = mSharedPref.edit();
                    mSEditor.putString(Config.REGISTRATIONTOKEN, token);
                    mSEditor.apply();

                    //Toast.makeText(getApplicationContext(), "My token: " + token, Toast.LENGTH_LONG).show();
                    //Log.w("tokenz", token);
                } else if (intent.getAction().endsWith(MyGCMRegistrationIntentService.REGISTRATION_ERROR)){
                    Toast.makeText(getApplicationContext(), "MYGCMRegistration Error!!!", Toast.LENGTH_LONG).show();
                } else {
                    //to be defined
                }
            }
        };

        /*
            check status of Google Play Service in the device
         */
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultCode) {
            //check types of error
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google play service is not installed or enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device doesn't support Google play service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Start the service
            Intent intent = new Intent(getApplicationContext(), MyGCMRegistrationIntentService.class);
            startService(intent);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                /*
                Show sign up alert dialog box
                 */
                showSignUpAlertDialog();
            }
        });

        mSignupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Show sign up alert dialog box
                 */
                showSignUpAlertDialog();
            }
        });

        //Log.w("NavigationActivity", "onResume");
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(MyGCMRegistrationIntentService.REGISTRATION_SUCCESS));

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(MyGCMRegistrationIntentService.REGISTRATION_ERROR));

    }

    @Override
    protected void onPause() {
        super.onPause();

        //Log.w("NavigationActivity", "onPause");
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private void showSignUpAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Signup.this, R.style.AlertDialogTheme);
        LayoutInflater inflater = Signup.this.getLayoutInflater();
        View view = inflater.inflate(R.layout.signup_alert_dialog, null);
        builder.setView(view);

        final EditText m_Username = (EditText) view.findViewById(R.id.sUsernameID);
        final EditText m_EmailAddress = (EditText) view.findViewById(R.id.sEmailID);
        final EditText m_Password = (EditText) view.findViewById(R.id.sPasswordID);
        final EditText m_ConfirmPassword = (EditText) view.findViewById(R.id.sConfirmPasswordID);

        builder.setTitle("Create an account")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Signup.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String username = m_Username.getText().toString().trim();
                        String email = m_EmailAddress.getText().toString().trim();
                        String password = m_Password.getText().toString().trim();
                        String confirmPassword = m_ConfirmPassword.getText().toString().trim();

                                /*
                                Sign up to the app
                                 */
                        signUp(username, password, confirmPassword, email);

                    }
                });
        AlertDialog dialog = builder.create();
        /*
        Show keyboard
         */
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }

    private void signUp(final String m_username, final String m_password, final String m_confirmPassword, final String m_emailaddress) {

        /*
         check for the network
         */
        if (!Utility.isConnectionAvailable(Signup.this)) {
            Utility.showAlertMessage(Signup.this);

        } else {

            mSharedPref = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
            String registration_token = mSharedPref.getString(Config.REGISTRATIONTOKEN, "Registration token not found!");

            if (!m_username.isEmpty() && !m_emailaddress.isEmpty() && !m_password.isEmpty() && !m_confirmPassword.isEmpty()) {

                if (m_username.length() >= 10 || m_password.length() < 5) {
                    Toast.makeText(getApplicationContext(), "Your username can be 10 characters long and password must be longer than 4 characters.", Toast.LENGTH_LONG).show();
                } else {
                    mSharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                    mEditor = mSharedPreferences.edit();
                    mEditor.putString(Config.USERNAMEKEY, m_username);
                    mEditor.apply();

                    RequestBody body = new FormBody.Builder()
                            .add("username", m_username)
                            .add("email", m_emailaddress)
                            .add("password", m_password)
                            .add("confirmpassword", m_confirmPassword)
                            .add("registrationToken", registration_token)
                            .build();

                    Request request = new Request.Builder()
                            .url(Config.URL_USER_SIGNUP_1)
                            .post(body)
                            .build();

                    Call call = client.newCall(request);
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {

                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {

                            if (response.isSuccessful()) {
                                //show the progress bar

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        signupProgressbar.setVisibility(View.VISIBLE);
                                    }
                                });

                                String mResponse = response.body().string();

                                try {
                                    JSONObject jsonObject = new JSONObject(mResponse);

                                    if (jsonObject.names().get(0).equals("success")) {

                                        final String mSuccess = jsonObject.getString("success");

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                //hide the progress bar
                                                signupProgressbar.setVisibility(View.GONE);
                                                Toast.makeText(getApplicationContext(), "Success: " + mSuccess, Toast.LENGTH_SHORT).show();

                                                    /*
                                                    Navigate to Navigation page
                                                     */
                                                Intent intent = new Intent(Signup.this, ProfileActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);

                                                    /*
                                                    Slide the activity
                                                     */
                                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                            }
                                        });



                                    } else {

                                        final String mError = jsonObject.getString("error");

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                signupProgressbar.setVisibility(View.GONE);
                                                Toast.makeText(getApplicationContext(), "Error: " + mError, Toast.LENGTH_SHORT).show();

                                                    /*
                                                    Remove all the shared preference values, so when the back button is tapped, user is not able to access NavigationActivity rather
                                                    displayed with log in MainActivity
                                                     */
                                                mSharedPref = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                                                mSEditor = mSharedPref.edit();
                                                mSEditor.remove(Config.REGISTRATIONTOKEN);
                                                mSEditor.apply();

                                                mSharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                                                mEditor = mSharedPreferences.edit();
                                                mEditor.remove(Config.USERNAMEKEY);
                                                mEditor.apply();

                                            }
                                        });

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }


                        }
                    });
                }

            } else {

                Toast.makeText(getApplicationContext(), "All fields are required!", Toast.LENGTH_SHORT).show();
            }
        }

    }

}


