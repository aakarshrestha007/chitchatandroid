package rotate.com.aakarshrestha.rotate.Helper;

import android.os.StrictMode;

public class BuildVersionCheck {

    public void checkBuildVersion() {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

}


