package rotate.com.aakarshrestha.rotate.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import rotate.com.aakarshrestha.rotate.R;
import rotate.com.aakarshrestha.rotate.UI.FindFriendActivity;
import rotate.com.aakarshrestha.rotate.Helper.UserProfile;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends BaseAdapter {

    private static List<UserProfile> mArrayList = new ArrayList<>();
    private LayoutInflater mInflater;
    Context mContext;
    final SharedPreferences mSharedPreferences;

    public UserAdapter(List<UserProfile> arrayList, Context context) {
        mArrayList = arrayList;
        mContext = context;
        mSharedPreferences = mContext.getSharedPreferences(FindFriendActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        mInflater = LayoutInflater.from(this.mContext);
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_view, null);
            holder = new ViewHolder();
            holder.mCheckedTextView = (CheckedTextView) convertView.findViewById(R.id.checkedTextViewID);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mCheckedTextView.setChecked(mSharedPreferences.getBoolean(mArrayList.get(position).getUsername(), true));

        holder.mCheckedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckedTextView) v).toggle();

                SharedPreferences.Editor editor = mSharedPreferences.edit();

                if (holder.mCheckedTextView.isChecked()) {
                    holder.mCheckedTextView.setChecked(false);
                    editor.putBoolean(mArrayList.get(position).getUsername(), false);
                    editor.apply();
                } else {
                    holder.mCheckedTextView.setChecked(true);
                    editor.putBoolean(mArrayList.get(position).getUsername(), true);
                    editor.apply();
                }
            }
        });

        holder.mCheckedTextView.setText(mArrayList.get(position).getUsername().toString());

        return convertView;
    }

    public class ViewHolder {

        CheckedTextView mCheckedTextView;

    }
}


