package rotate.com.aakarshrestha.rotate.Helper;

public class Config {

    static final String host = "99.70.246.172";/* "192.168.1.73";*/
    static final String port = "80";/* "8880";*/
    static final String folderName = "chitChat";

    public static final String token = "&token=7kPbftbrcpAKs9K";

    public static final String SHAREDPREFERENCES = "chitchatpreferences";
    public static final String USERNAMEKEY = "mUsernameKey";
    public static final String REGISTRATIONTOKEN = "registrationtoken";
    public static final String MESSAGE_KEY = "MESSAGE";
    public static final String MESSAGE_VALUE = "msgValue";

    public static final String URL_BASIC = "http://"+host+":"+port+"/"+folderName+"/";
    public static final String URL = URL_BASIC + "user_finds_friend.php?username=";
    public static final String URL_USER_PROFILE = URL_BASIC + "users.php?username=";
    public static final String URL_MAKE_FRIENDS = URL_BASIC + "make_friend.php";
    public static final String URL_DELETE_FRIENDS = URL_BASIC + "delete_friendship.php";
    public static final String URL_FRIENDSHIP_TAG = URL_BASIC + "friendship_tag.php?username=";
    public static final String URL_FRIENDSHIP_TAG_ONE = URL_BASIC + "friendship_tag_one.php?username=";
    public static final String URL_USER_CONTROLLER = URL_BASIC + "user_controller.php";
    public static final String URL_USER_SIGNUP_1 = URL_BASIC + "user_signup.php";
    public static final String URL_ADD_IMAGE_PATH = URL_BASIC + "add_image_path.php";
    public static final String URL_IMAGE_UPLOAD = URL_BASIC + "uploadImage.php";
    public static final String URL_UPDATE_USER_GCM = URL_BASIC + "update_gcm_regid.php";
    public static final String URL_SHOW_FRIEND_REQUEST= URL_BASIC + "show_friend_request.php?username=";
    public static final String URL_ACCEPT_FRIEND_REQUEST = URL_BASIC + "accept_friendrequest.php";
    public static final String URL_POST_COMMENT = URL_BASIC + "post_comment.php";
    public static final String URL_GET_POST_COMMENT = URL_BASIC + "get_post_comment.php?friendname=";
    public static final String URL_UPDATE_EMAIL = URL_BASIC + "update_email_address.php";
    public static final String URL_UPDATE_PASSWORD = URL_BASIC + "update_password.php";

    public static String myAppName = "/NJ Folder";

}
