package rotate.com.aakarshrestha.rotate.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import rotate.com.aakarshrestha.rotate.Helper.Comment;
import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;
import rotate.com.aakarshrestha.rotate.UI.MessageActivity;
import rotate.com.aakarshrestha.rotate.UI.ProfileActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MyGetCommentsAdapter extends BaseAdapter {
    List<Comment> mCommentList = new ArrayList<>();
    Context mContext;
    LayoutInflater mInflater;
    ViewHolder mViewHolder;

    public SharedPreferences friendGCMPreferences, myPreference;
    public SharedPreferences.Editor friendEditor;

    public MyGetCommentsAdapter(List<Comment> commentList, Context context) {
        mCommentList = commentList;
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mCommentList.size();
    }

    @Override
    public Object getItem(int position) {
        return mCommentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.my_profile_listview_item, parent, false);
            mViewHolder = new ViewHolder();
            mViewHolder.mCommenterName = (TextView) convertView.findViewById(R.id.my_commenter_nameID);
            mViewHolder.mCommentText = (TextView) convertView.findViewById(R.id.my_commentID);
            mViewHolder.mCommentTimeStamp = (TextView) convertView.findViewById(R.id.my_timestampID);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        Comment comment = mCommentList.get(position);

        mViewHolder.mCommenterName.setText(comment.getCommenterName());
        mViewHolder.mCommentText.setText(comment.getComment());

        /*
        When the name of the commenter is clicked, navigate to the message activity
         */
        mViewHolder.mCommenterName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectionAvailable(mContext)) {
                    Utility.showAlertMessage(mContext);
                } else {
                    String nameOfCommenter = mCommentList.get(position).getCommenterName();

                    myPreference = mContext.getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                    String my_name = myPreference.getString(Config.USERNAMEKEY, "");
                    /*
                    Call the navigateToMessageActivity method
                     */
                    navigateToMessageActivity(nameOfCommenter, my_name);
                }

            }
        });

        String theTimeStampUserProfile = comment.getTimeStamp();
        mViewHolder.mCommentTimeStamp.setText(Utility.timeFormatter(theTimeStampUserProfile));
        mViewHolder.mCommentText.setTextColor(Color.BLACK);

        return convertView;
    }

    static class ViewHolder {

        public TextView mCommenterName;
        public TextView mCommentText;
        public TextView mCommentTimeStamp;

    }

    /*
    Navigate to the message activity
     */
    private void navigateToMessageActivity(final String friendName, final String u_name) {
        //make a webservice call to get the gcm registration id of the friend.
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(Config.URL_USER_PROFILE + friendName + Config.token)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String friendRegistrationToken = response.body().string();

                    try {
                        JSONArray jsonArray = new JSONArray(friendRegistrationToken);

                        for (int i=0; i<jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            final String gcm_token = jsonObject.getString("gcm_regid").trim();

                            friendGCMPreferences = mContext.getSharedPreferences("friendGCMPreferences", Context.MODE_PRIVATE);
                            friendEditor = friendGCMPreferences.edit();
                            friendEditor.putString("fgcmpreferences", gcm_token);
                            friendEditor.apply();

                            /*
                                if the token is not empty, navigate to MessageActivity otherwise show an error.
                             */
                            if (!gcm_token.isEmpty()) {
                                Intent intent = new Intent(mContext, MessageActivity.class);
                                intent.putExtra("friendname", friendName);
                                intent.putExtra("myname", u_name);
                                intent.putExtra("gcmToken", gcm_token);
                                mContext.startActivity(intent);

                                /*
                                Slide the activity
                                */
                                ProfileActivity.mProfileActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
    }
}


