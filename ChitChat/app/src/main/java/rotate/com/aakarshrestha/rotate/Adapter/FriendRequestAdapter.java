package rotate.com.aakarshrestha.rotate.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import rotate.com.aakarshrestha.rotate.Helper.FriendRequest;
import rotate.com.aakarshrestha.rotate.Helper.FriendsList;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;
import rotate.com.aakarshrestha.rotate.UI.FriendRequestActivity;

import java.util.ArrayList;
import java.util.List;

public class FriendRequestAdapter extends BaseAdapter {

    List<FriendRequest> mFriendRequestList = new ArrayList<>();
    Context mContext;
    String mUser_name_req;

    Activity mActivity;

    public FriendRequestAdapter(List<FriendRequest> friendRequestList, Context context, String user_name_req, Activity activity) {
        mFriendRequestList = friendRequestList;
        mContext = context;
        mUser_name_req = user_name_req;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mFriendRequestList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFriendRequestList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder holder;

        String name = mFriendRequestList.get(position).toString();
        final String[] nameArray = name.split(" ", 2);

        if (convertView == null) {

            final LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (nameArray[1].equals(mUser_name_req)) {
                convertView = inflater.inflate(R.layout.friend_request_pending_item_view, parent, false);
            }
            else {
                convertView = inflater.inflate(R.layout.friend_request_item_view, parent, false);
            }

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

            /*
                Make the buttons clickable when buttons are displayed in the friend request activity.
                Placing this buttons code here prevents NPE on setOnClickListener method.
            */

            if (nameArray[0].equals(mUser_name_req)) {

                final FriendsList friendslist = new FriendsList();

                holder.confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        /*
                            check for the network connection
                         */
                        if (!Utility.isConnectionAvailable(mActivity)) {
                            Utility.showAlertMessage(mActivity);
                        } else {
                            String name = mFriendRequestList.get(position).toString();
                            String[] nameArray = name.split(" ", 2);
                            String username = nameArray[0];
                            String friendname = nameArray[1];

                            friendslist.acceptFriendRequest(username, friendname);

                            mFriendRequestList.remove(position);

                            //Toast.makeText(mContext, nameArray[1], Toast.LENGTH_LONG).show();

                            notifyDataSetChanged();

                            refreshFriendRequest(mFriendRequestList);
                        }

                    }
                });

                holder.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        /*
                            check for the network connection
                         */
                        if (!Utility.isConnectionAvailable(mActivity)) {
                            Utility.showAlertMessage(mActivity);
                        } else {
                            String name = mFriendRequestList.get(position).toString();
                            String[] nameArray = name.split(" ", 2);
                            String user_name = nameArray[0];
                            String friend_name = nameArray[1];

                            friendslist.rejectFriendRequest(user_name, friend_name);

                            mFriendRequestList.remove(position);
                            notifyDataSetChanged();

                            refreshFriendRequest(mFriendRequestList);
                        }

                    }

                });

            }

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        /*
        Show pending message with the friend's name to whom the friend request is sent
         */
        if (nameArray[1].equals(mUser_name_req)) {
            holder.friendRequestName.setText(nameArray[0]);
            holder.friendRequestName.setTextColor(Color.DKGRAY);
        } else {
            holder.friendRequestName.setText(nameArray[1]);
            holder.friendRequestName.setTextColor(Color.BLACK);
        }

        return convertView;
    }

    static class ViewHolder {

        public TextView friendRequestName, friendReqPending, pending, confirm, delete;
        //public ImageButton checkButton, crossButton;

        public ViewHolder(View v) {
            friendRequestName = (TextView) v.findViewById(R.id.friend_request_nameID);
            //checkButton = (ImageButton) v.findViewById(R.id.checkImageButtonID);
            //crossButton = (ImageButton) v.findViewById(R.id.crossImageButtonID);
            confirm = (TextView) v.findViewById(R.id.confirmID);
            delete = (TextView) v.findViewById(R.id.deleteID);

            friendReqPending = (TextView) v.findViewById(R.id.friend_request_nameID);
            pending = (TextView) v.findViewById(R.id.friendRequestPendingID);
        }
    }

    public void remove(int position) {
        mFriendRequestList.remove(mFriendRequestList.get(position));
        notifyDataSetChanged();
    }

    public void refreshFriendRequest(List<FriendRequest> mFriendRequestList) {
        /*
            Call the intent here to prevent the pending user getting the buttons in the layout.
            It reloads the activity and prevents from displaying stale layout.
         */

        this.mFriendRequestList.clear();
        this.mFriendRequestList.addAll(mFriendRequestList);
        notifyDataSetChanged();

        Intent intent = new Intent(mContext, FriendRequestActivity.class);

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mContext.startActivity(intent);

        mActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
}

