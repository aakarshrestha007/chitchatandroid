package rotate.com.aakarshrestha.rotate.Service;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

public class MyGCMTokenRefreshListenerService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {

        Intent intent = new Intent(this, MyGCMRegistrationIntentService.class);
        startService(intent);

    }
}

