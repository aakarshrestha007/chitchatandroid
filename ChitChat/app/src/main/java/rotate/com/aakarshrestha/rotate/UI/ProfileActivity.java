package rotate.com.aakarshrestha.rotate.UI;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import rotate.com.aakarshrestha.rotate.Adapter.MyGetCommentsAdapter;
import rotate.com.aakarshrestha.rotate.Helper.Comment;
import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.Conversation;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileActivity extends AppCompatActivity {

    protected ImageView mImageView;
    private TextView mProfileUsername;
    private TextView mProfileEmail;
    private TextView mProfileDate;
    private ImageView mProfileImage;
    protected ImageButton mEditProfile;
    protected ImageButton mProfileCameraButton;

    protected ListView mListView;
    public List<Comment> mCommentList;
    public Conversation mConversation;
    protected MyGetCommentsAdapter mMyGetCommentsAdapter;

    protected SharedPreferences sharedPreferences;
    protected SharedPreferences emailPreference;
    protected SharedPreferences.Editor emailEditor;

    OkHttpClient client = new OkHttpClient();

    public static final int TAKE_PHOTO_REQUEST = 0;
    public static final int READ_PHOTO_REQUEST = 13;
    public static final int MEDIA_TYPE_IMAGE = 4;
    public static final int FILE_SIZE_LIMIT = 1024*1024*2; //2MB
    public String imageName, encodedString;
    public File mediaStorageDir;
    public Uri mMediaUri;
    public Bitmap mBitmap;

    protected LinearLayout mLinearLayout2;

    public static ProfileActivity mProfileActivity;

    public IntentFilter postCommentGcmIntentFilter;
    /*
    Broadcast receiver for the post comment
     */

    BroadcastReceiver mPostCommentBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String friend_name_post = intent.getExtras().getString("post_comment_friendname");
            String my_name_post = intent.getExtras().getString("post_comment_myname");

            if (friend_name_post != null && my_name_post != null) {
                /*
                Get the comment posted by other friends
                 */
                getMyComments(my_name_post);
            }

        }
    };

    /*
        Profile menu
     */
    protected DialogInterface.OnClickListener mDialogListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            switch (which) {

                case 0:

                    final AlertDialog.Builder emailBuilder = new AlertDialog.Builder(ProfileActivity.this, R.style.AlertDialogTheme)
                            .setTitle("Update Email Address");

                    LayoutInflater emailInflater = ProfileActivity.this.getLayoutInflater();
                    View emailView = emailInflater.inflate(R.layout.update_email, null);
                    emailBuilder.setView(emailView);

                    final EditText mEmailAddress = (EditText) emailView.findViewById(R.id.newEmailAddressID);
                    final TextView mCurrentEmailAddress = (TextView) emailView.findViewById(R.id.currentEmailAddressID);

                    emailPreference = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                    final String currentEmail = emailPreference.getString("emailAddress", "No email address found...");
                    mCurrentEmailAddress.setText(currentEmail);

                    emailBuilder.setNegativeButton("Cancel", null)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                                    String usernameToUpdateEmail = sharedPreferences.getString(Config.USERNAMEKEY, "");
                                    String newEmailAddress = mEmailAddress.getText().toString().trim();

                                    if (!newEmailAddress.isEmpty()) {

                                /*
                                Calling the method to update email address.
                                 */
                                        updateEmailAddress(usernameToUpdateEmail, currentEmail, newEmailAddress);
                                        //Toast.makeText(ProfileActivity.this, emailAddress + " " +usernameToUpdateEmail, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(ProfileActivity.this, "Email address field is empty.", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });

                    AlertDialog emailDialog = emailBuilder.create();
                    /*
                    Show keyboard
                     */
                    emailDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    emailDialog.show();
                    break;

                case 1:

                    AlertDialog.Builder passwordBuilder = new AlertDialog.Builder(ProfileActivity.this, R.style.AlertDialogTheme)
                            .setTitle("Update Password");

                    LayoutInflater passwordInflater = ProfileActivity.this.getLayoutInflater();
                    View passwordView = passwordInflater.inflate(R.layout.update_password, null);
                    passwordBuilder.setView(passwordView);

                    final EditText mCurrentPassword = (EditText) passwordView.findViewById(R.id.currentPasswordID);
                    final EditText mNewPassword = (EditText) passwordView.findViewById(R.id.newPasswordID);
                    final EditText mConfirmPassword = (EditText) passwordView.findViewById(R.id.confirmNewPasswordID);

                    passwordBuilder.setNegativeButton("Cancel", null)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    String currentPassword = mCurrentPassword.getText().toString();
                                    String newPassword = mNewPassword.getText().toString();
                                    String confirmPassword = mConfirmPassword.getText().toString();

                                    sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                                    String usernameToUpdatePassword = sharedPreferences.getString(Config.USERNAMEKEY, "");

                                    if (!currentPassword.isEmpty() && !newPassword.isEmpty() && !confirmPassword.isEmpty()) {

                                        if(newPassword.length() >= 5 && newPassword.equals(confirmPassword)) {

                                            /*
                                            Update the password of the user
                                             */
                                            updatePassword(usernameToUpdatePassword, currentPassword, newPassword, confirmPassword);
                                            //Toast.makeText(getApplicationContext(), "Password updated successfully!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Password must be atleast 5 characters long and must match new and confirm password values.", Toast.LENGTH_LONG).show();
                                        }

                                    } else {
                                        Toast.makeText(getApplicationContext(), "All fields are required!", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });

                    AlertDialog passwordDialog = passwordBuilder.create();
                    /*
                    Show keyboard
                     */
                    passwordDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    passwordDialog.show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mProfileActivity = this;

        mImageView = (ImageView) findViewById(R.id.profileimage_backarrowID);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, NavigationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

               /*
                Slide the activity
                */
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        mLinearLayout2 = (LinearLayout) findViewById(R.id.linearLayout2);

        mProfileUsername = (TextView) findViewById(R.id.profileunameID);
        mProfileEmail = (TextView) findViewById(R.id.profileemailID);
        mProfileDate = (TextView) findViewById(R.id.profiledateID);
        mProfileImage = (ImageView) findViewById(R.id.profileimageID);
        mEditProfile = (ImageButton) findViewById(R.id.editProfileID);
        mProfileCameraButton = (ImageButton) findViewById(R.id.profileCameraButtonID);

        mListView = (ListView) findViewById(R.id.myListViewID);
        mCommentList = new ArrayList<>();
        mConversation = new Conversation();

        /*
        Add action of the broadcast receiver
         */
        postCommentGcmIntentFilter = new IntentFilter();
        postCommentGcmIntentFilter.addAction("GCM_POST_COMMENT_RECEIVED_ACTION");

    }

    @Override
    protected void onResume() {
        super.onResume();

//        /*
//        Rotate the profile image
//         */
//        final Animation profileImageRotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
//
//        mLinearLayout2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mProfileImage.startAnimation(profileImageRotate);
//            }
//        });

        /*
            Getting the post in the user's profile activity
         */

        sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        String usernameToGetPost = sharedPreferences.getString(Config.USERNAMEKEY, "Not found!");

        getMyComments(usernameToGetPost);

        /*
            animation
         */
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                YoYo.with(Techniques.FlipOutX).duration(1000).playOn(mProfileImage);
//                YoYo.with(Techniques.FlipInX).duration(2000).playOn(mProfileImage);
//            }
//        });

        if (!Utility.isConnectionAvailable(ProfileActivity.this)) {
            Utility.showAlertMessage(ProfileActivity.this);
        } else {

            mEditProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this, R.style.AlertDialogTheme)
                            .setTitle("Settings")
                            .setItems(R.array.profile_options, mDialogListener);

                    AlertDialog dialog = builder.create();
                    dialog.show();

                    //Toast.makeText(getApplicationContext(), "Coming soon!!!", Toast.LENGTH_SHORT).show();
                }
            });

            /*
            Get the profile image of the user.
             */
            getProfileImage(usernameToGetPost);

        }


        /*
        Camera
         */

        mProfileCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectionAvailable(ProfileActivity.this)) {
                    Utility.showAlertMessage(ProfileActivity.this);
                } else {
                    /*
                    Check for the camera permission - Marshmallow
                     */

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            openCamera();
                        } else {
                            String[] permissionRequested = {Manifest.permission.CAMERA};
                            requestPermissions(permissionRequested, TAKE_PHOTO_REQUEST);
                        }
                    } else {
                        openCamera();
                    }
                }

            }
        });

        /*
        register the receiver for the post
         */
        registerReceiver(mPostCommentBroadCastReceiver, postCommentGcmIntentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        /*
        unregister the receiver for the post
         */
        unregisterReceiver(mPostCommentBroadCastReceiver);
    }

    /*
    Request the permission to use the camera - Marshmallow
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == TAKE_PHOTO_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera();
            } else {
                Toast.makeText(getApplicationContext(), "Permission required to use the camera.", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == READ_PHOTO_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                imageInputStream();
            } else {
                Toast.makeText(getApplicationContext(), "Permission required to read the content of camera.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mMediaUri = getOutputMediaFile(MEDIA_TYPE_IMAGE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mMediaUri);
        startActivityForResult(cameraIntent, TAKE_PHOTO_REQUEST);
    }

    //After taking the photo using the camera, sometimes the app orientation changes
    //that will throw the NPE error. So when the orientation changes, save the file url
    // in bundle as it will be null on screen orientation.
    //use onSaveInstanceState method
    //and to retrieve the file url, use onRestoreInstanceState method
    // IT SAVES A LOT OF HEADACHE!!!
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("media_uri", mMediaUri);
        outState.putString("clicked_image_name", imageName);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //get the file url
        mMediaUri = savedInstanceState.getParcelable("media_uri");
        imageName = savedInstanceState.getString("clicked_image_name");
    }

    private void getProfileImage(final String username) {

//        sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
//        final String username = sharedPreferences.getString(Config.USERNAMEKEY, "Not found!");

        Request request = new Request.Builder()
                .url(Config.URL_USER_PROFILE+username+Config.token)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String uEmail = null;
                String uDate = null;
                String uImage = null;

                if (response.isSuccessful()) {

                    String mResponse = response.body().string();
                    try {

                        JSONArray jsonArray = new JSONArray(mResponse);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            uEmail = jsonObject.getString("email");
                            uDate = jsonObject.getString("createdDate");
                            uImage = jsonObject.getString("image_path");

                            //Log.v("ATC", uEmail + " " + uDate + " " + uImage);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                //set the username, email and created date of the user here

                final String finalUEmail = uEmail;
                final String finalUDate = uDate;
                final String finalUImageWIthURL = Config.URL_BASIC + uImage;
                final String finalUImage = uImage;
                final String copyOfProfileImagePath = uImage;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            mProfileUsername.setText("Hey, I am " + username);
                            mProfileEmail.setText(finalUEmail);
                            mProfileDate.setText("Member since " + finalUDate);

                            emailPreference = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                            emailEditor = emailPreference.edit();
                            emailEditor.putString("emailAddress", finalUEmail);
                            emailEditor.apply();

                            //set a profile image otherwise set default image
                            if (finalUImage.equals("null") || copyOfProfileImagePath.equals("null")) {
                                Picasso.with(getApplicationContext()).load(R.drawable.dummy).into(mProfileImage);
                            } else {
                                Picasso.with(getApplicationContext()).load(finalUImageWIthURL).into(mProfileImage);

                                    /*
                                    Display the profile image in the alert dialog
                                    */
                                mProfileImage.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this, R.style.AlertDialogTheme)
                                                        .setCancelable(true);

                                                LayoutInflater inflater = LayoutInflater.from(ProfileActivity.this);
                                                View view = inflater.inflate(R.layout.alert_profile_image, null);
                                                builder.setView(view);

                                                ImageView imageView = (ImageView) view.findViewById(R.id.alertProfileImageID);
                                                Picasso.with(getApplicationContext()).load(finalUImageWIthURL).into(imageView);

                                                builder.setNeutralButton("Close", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });

                                                AlertDialog dialog = builder.create();
                                                dialog.show();
                                            }
                                        });
                                    }
                                });
                            }

                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });

    }

    private Uri getOutputMediaFile(int mediaType) {

        if (isExternalStorageAvailable()) {

            String appName = ProfileActivity.this.getString(R.string.app_name);
            mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), appName);

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdir()) {
                    mediaStorageDir.mkdir();
                    //return null;
                }
            }

            File mediaFile;
            Date now = new Date();
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(now);
            String path = mediaStorageDir.getPath() + File.separator;
            imageName = "IMG_"+ timeStamp + ".jpg";

            if (mediaType == MEDIA_TYPE_IMAGE) {
                mediaFile = new File(path + imageName);
            } else {
                return null;
            }

            mMediaUri = Uri.fromFile(mediaFile);

        }

        return mMediaUri;
    }

    private boolean isExternalStorageAvailable() {

        boolean isMounted;
        String state = Environment.getExternalStorageState();

        isMounted = state.equals(Environment.MEDIA_MOUNTED);

        return isMounted;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PHOTO_REQUEST) {

            if (resultCode == RESULT_OK) {

                /*
                Check for the permission to use the camera
                 */

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        imageInputStream();
                    } else {
                        String[] permission_requested = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permission_requested, READ_PHOTO_REQUEST);
                    }
                } else {
                    imageInputStream();
                }

            }

        }

    }

    private void imageInputStream() {
        InputStream inputStream = null;
        int fileSize = 0;

        try {
            inputStream = getContentResolver().openInputStream(mMediaUri);
            assert inputStream != null;
            fileSize = inputStream.available();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {

                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (fileSize >= FILE_SIZE_LIMIT) {
            Toast.makeText(ProfileActivity.this, "Image size is too big.", Toast.LENGTH_LONG).show();
            return;
        } else {
            EncodeImageTask encodeImageTask = new EncodeImageTask();
            encodeImageTask.execute();
        }
    }

    private class EncodeImageTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            //initializing Bitmap and getting path of the image file
            mBitmap = BitmapFactory.decodeFile(mMediaUri.getPath());

            if (mBitmap != null) {
                //Setting the image orientation to 270 degree so that the image is not displayed sideways
                Matrix matrix = new Matrix();
                matrix.postRotate(270);

                mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
            }

            //convert the image to the byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bos);

            byte[] byteArray = bos.toByteArray();

            if (byteArray.length > 0) {
                encodedString = Base64.encodeToString(byteArray, 0);
            } else {
                encodedString = "";
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            giveMeImageEncodedString();
        }
    }

    private void giveMeImageEncodedString() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder uploadImageBuilder = new AlertDialog.Builder(ProfileActivity.this, R.style.AlertDialogTheme)
                        .setTitle("Upload Image")
                        .setMessage("Are you sure you want to upload this image?")
                        .setNegativeButton("Cancel", null)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                                final String username = sharedPreferences.getString(Config.USERNAMEKEY, "");

                                String valueOfEncodedString = encodedString;
                                String nameOfTheImage = imageName;

                                Toast.makeText(getApplicationContext(), "Uploading the image...", Toast.LENGTH_SHORT).show();

                                RequestBody body = new FormBody.Builder()
                                        .add("username", username)
                                        .add("encoded_string", valueOfEncodedString)
                                        .add("image_name", nameOfTheImage)
                                        .build();

                                Request request = new Request.Builder()
                                        .url(Config.URL_IMAGE_UPLOAD)
                                        .post(body)
                                        .build();

                                Call call = client.newCall(request);
                                call.enqueue(new Callback() {
                                    @Override
                                    public void onFailure(Call call, IOException e) {

                                    }

                                    @Override
                                    public void onResponse(Call call, Response response) throws IOException {

                                        if (response.isSuccessful()) {

                                            String mResponse = response.body().string();

                                            try {
                                                JSONObject jsonObj = new JSONObject(mResponse);

                                                if (jsonObj.names().get(0).equals("success")) {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {

                                                            addImagePathToFriendListTable(username);

                                                            try {
                                                                Thread.sleep(2000);
                                                            } catch (InterruptedException e) {
                                                                e.printStackTrace();
                                                            }

                                                            //getProfileImage(username);
                                                            Toast.makeText(getApplicationContext(), "Image uploaded successfully!", Toast.LENGTH_SHORT).show();

                                                            Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
                                                            startActivity(intent);
                                                        }
                                                    });

                                                } else if (jsonObj.names().get(0).equals("error")) {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Toast.makeText(getApplicationContext(), "Error uploading the image!", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });

                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }

                                        response.body().close();
                                    }
                                });
                            }
                        });

                AlertDialog uploadImageDialog = uploadImageBuilder.create();
                uploadImageDialog.show();
            }
        });

    }

    private void addImagePathToFriendListTable(String username) {

        RequestBody body = new FormBody.Builder()
                .add("username", username)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_ADD_IMAGE_PATH)
                .post(body)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String mResponse = response.body().string();

                    try {
                        JSONObject jsObj = new JSONObject(mResponse);

                        if (jsObj.names().get(0).equals("success")) {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

    }

    private List<Comment> getCommentListValue(Comment[] commentsArray) {

        List<Comment> list = Arrays.asList(commentsArray);
        return list;

    }

    public void getMyComments(String myUserNameValue) {
        getPostCommentList(myUserNameValue);
    }

    public void getPostCommentList(String myUsername) {

        GetPostCommentTask getPostCommentTask = new GetPostCommentTask();
        getPostCommentTask.execute(myUsername);

    }

    private class GetPostCommentTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            getPostCommentWebService(params[0]);
            return null;
        }
    }

    public void getPostCommentWebService(String usernameToGetPost) {

        Request myRequest = new Request.Builder()
                .url(Config.URL_GET_POST_COMMENT + usernameToGetPost + Config.token)
                .build();

        Call myCall = client.newCall(myRequest);
        myCall.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String mResponse = response.body().string();

                    try {

                        mCommentList = getCommentListValue(mConversation.getComments(mResponse));
                        if (!mCommentList.isEmpty()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mMyGetCommentsAdapter = new MyGetCommentsAdapter(mCommentList, ProfileActivity.this);
                                    mListView.setAdapter(mMyGetCommentsAdapter);
                                    mMyGetCommentsAdapter.notifyDataSetChanged();
                                }
                            });
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    response.body().close();
                }

            }
        });

    }

    private void updateEmailAddress(String username, String emailAddress, String newEmailAddress) {

        RequestBody body = new FormBody.Builder()
                .add("username", username)
                .add("email_address", emailAddress)
                .add("new_email_address", newEmailAddress)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_UPDATE_EMAIL)
                .post(body)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String mResponse = response.body().string();

                    try {
                        final JSONObject jsonObject = new JSONObject(mResponse);

                        if (jsonObject.names().get(0).equals("success")) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Toast.makeText(ProfileActivity.this, "Success: " + jsonObject.getString("success"), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Toast.makeText(ProfileActivity.this, "Error: " + jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

    }

    private void updatePassword(String username, String currentPassword, String newPassword, String confirmPassword) {

        RequestBody body = new FormBody.Builder()
                .add("username", username)
                .add("current_password", currentPassword)
                .add("new_password", newPassword)
                .add("confirm_password", confirmPassword)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_UPDATE_PASSWORD)
                .post(body)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String passwordResponse = response.body().string();

                    try {
                        final JSONObject passwordJsonObj = new JSONObject(passwordResponse);

                        if (passwordJsonObj.names().get(0).equals("success")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Toast.makeText(getApplicationContext(), "Success: " + passwordJsonObj.getString("success"), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Toast.makeText(getApplicationContext(), "Error: " + passwordJsonObj.getString("error"), Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        /*
        This method prevents the app being killed when back button is clicked.
         */

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }

}


