package rotate.com.aakarshrestha.rotate.UI;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import rotate.com.aakarshrestha.rotate.Adapter.MessageAdapter;
import rotate.com.aakarshrestha.rotate.Helper.AutoLogin;
import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.Conversation;
import rotate.com.aakarshrestha.rotate.Helper.Messages;
import rotate.com.aakarshrestha.rotate.Helper.URLS;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MessageActivity extends AppCompatActivity {

    private TextView mUsernameToolID;
    public ImageView mImageView;
    public ImageView mBgImageView;
    private Button mButton;
    private EditText mEditText;
    private ListView mListView;
    public ProgressBar messageProgressBar;

    public MessageAdapter mMessageAdapter;
    public List<Messages> mMessagesArrayList = new ArrayList<>();

    public Conversation mConversation = new Conversation();

    public SharedPreferences sharedPreferences;
    public SharedPreferences msgSharedPreferences;
    public SharedPreferences.Editor msgEditor;

    public IntentFilter gcmFilter;

    private BroadcastReceiver gcmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String myname = intent.getExtras().getString("friendname");
            String friendName = intent.getExtras().getString("myname");
            String broadcastMessage = intent.getExtras().getString("messageFromService");

            //Reload the getMessage() method get all the message in the list view.
            //we are getting the realtime data

            if (myname !=null && friendName != null && broadcastMessage != null) {

                //getMessage(URLS.getFriendMessageUrl(myname, friendName));

                if (AutoLogin.loggedInAlready(getApplicationContext())) {
                    BroadcastAsyncTask broadcastAsyncTask = new BroadcastAsyncTask();
                    broadcastAsyncTask.execute(myname, friendName);

                }

            }
        }
    };

    private class BroadcastAsyncTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            getMessage(URLS.getFriendMessageUrl(params[0], params[1]));

            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        mBgImageView = (ImageView)findViewById(R.id.bgImageID);
        mImageView = (ImageView) findViewById(R.id.backArrowIDimageMessage);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MessageActivity.this, NavigationActivity.class);
                startActivity(intent);

                /*
                Slide the activity
                */
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        mUsernameToolID = (TextView) findViewById(R.id.usernameToolID);
        mUsernameToolID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUsernameToolID.setTextColor(Color.WHITE);
                getFriendProfile();
            }
        });

        //hide progress bar
        messageProgressBar = (ProgressBar) findViewById(R.id.messageProgressBarID);

        /*
        Add action of the broadcast receiver
         */
        gcmFilter = new IntentFilter();
        gcmFilter.addAction("GCM_RECEIVED_ACTION");

    }



    @Override
    protected void onResume() {
        super.onResume();

        //hide the progress bar
        messageProgressBar.setVisibility(View.GONE);

        //get intent
        Intent intent = getIntent();

        final String myname = intent.getStringExtra("myname");
        final String friendname = intent.getStringExtra("friendname");
        final String gcmValue = intent.getStringExtra("gcmToken");

        final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.activityMessageID);

        //setting up the background image of friend in the message
        setBackgroundImage(friendname, relativeLayout);

        //set recipient's name on the toolbar
        mUsernameToolID.setText(friendname + "'s Profile");

        mButton = (Button) findViewById(R.id.buttonSendID);
        mEditText = (EditText) findViewById(R.id.messageEditTextID);
        //mEditText.setText(friendname);

        mEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //animation
                YoYo.with(Techniques.FlipOutX).duration(100).playOn(mButton);
                YoYo.with(Techniques.FlipInX).duration(1000).playOn(mButton);
                mButton.setVisibility(View.VISIBLE);
            }
        });

        mListView = (ListView) findViewById(R.id.message_listView);

        /*
            get message send to and from users
         */
        getMessage(URLS.getFriendMessageUrl(myname, friendname));

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messages = mEditText.getText().toString();
                String messages_value = messages.trim();


                if (!Utility.isConnectionAvailable(MessageActivity.this)) {
                    Utility.showAlertMessage(MessageActivity.this);

                    //show the progress bar
                    messageProgressBar.setVisibility(View.VISIBLE);

                } else {

                    if (!messages_value.isEmpty()) {

                        msgSharedPreferences = getSharedPreferences(Config.MESSAGE_KEY, Context.MODE_PRIVATE);
                        msgEditor = msgSharedPreferences.edit();
                        msgEditor.putString(Config.MESSAGE_VALUE, messages_value);
                        msgEditor.apply();

                        /*
                        Count the characters in the message
                         */
                        int counter = 0;
                        for (int i=0; i<messages_value.length(); i++) {
                            if ((Character.isLetterOrDigit(messages_value.charAt(i))) || (!Character.isLetterOrDigit(messages_value.charAt(i)))) {
                                counter++;
                            }
                        }

                        /*
                            Only send the message if the characters in the message is less than or equal to 140 characters.
                         */
                        if (counter <= 140) {
                            if (mConversation.convoCheckInBackground(URLS.MESSAGE_URL, myname, friendname, messages_value.replace("'", "\''"), gcmValue)) {
                                messageProgressBar.setVisibility(View.GONE);
                                mEditText.setText("");

                                /*
                                Remove the message saved in sharedPreferences;
                                 */
                                msgSharedPreferences = getSharedPreferences(Config.MESSAGE_KEY, Context.MODE_PRIVATE);
                                msgEditor = msgSharedPreferences.edit();
                                msgEditor.remove(Config.MESSAGE_VALUE);
                                msgEditor.apply();


                            } else {
                                //show the progress bar
                                messageProgressBar.setVisibility(View.VISIBLE);
                            }

                            YoYo.with(Techniques.FlipOutX).duration(100).playOn(mEditText);
                            YoYo.with(Techniques.FlipInX).duration(1000).playOn(mEditText);
                        } else {
                            Utility.showAlertMessageForCharacterMoreThan140(MessageActivity.this);

                            msgSharedPreferences = getSharedPreferences(Config.MESSAGE_KEY, Context.MODE_PRIVATE);
                            String msg = msgSharedPreferences.getString(Config.MESSAGE_VALUE, "");

                            /*
                            Show the typed message from the sharedPreferences
                             */

                            if (msg.length() > 0) {
                                mEditText.setText(msg);
                            }

                        }

                    }

                    //load the message again
                    getMessage(URLS.getFriendMessageUrl(myname, friendname));

                }

            }
        });

        /*
            register the message
         */
        registerReceiver(gcmReceiver, gcmFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();

        /*
            this method unregister the message
         */
        unregisterReceiver(gcmReceiver);
    }

    private void getFriendProfile() {

        Intent intent = getIntent();
        String my_name = intent.getStringExtra("myname");
        String friend_name = intent.getStringExtra("friendname");
        String gcm_token = intent.getStringExtra("gcmToken");

        Intent friendIntent = new Intent(getApplicationContext(), FriendProfileActivity.class);
        friendIntent.putExtra("mynameYo", my_name);
        friendIntent.putExtra("friendNameYo", friend_name);
        friendIntent.putExtra("friendGcmToken", gcm_token);
        startActivity(friendIntent);
        /*
        Slide the activity
        */
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    private void setBackgroundImage(final String friendname, final RelativeLayout relativeLayout) {

        runOnUiThread(new Runnable() {

//            File file = new File(Environment.getExternalStorageDirectory() + File.separator + Config.myAppName);
//            File fileName = new File(file, friendname + ".jpg");

            File file = getExternalFilesDir(Environment.DIRECTORY_PICTURES + File.separator + Config.myAppName);
            File fileName = new File(file, friendname + ".jpg");

            @Override
            public void run() {
                Picasso.with(getApplicationContext()).load(fileName).resize(700, 300).into(new com.squareup.picasso.Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        relativeLayout.setBackground(new BitmapDrawable(getApplication().getResources(), bitmap));
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            }
        });

    }

    public void getMessage(String friendmessageurl) {
        refreshMessageList(friendmessageurl);
    }

    public void refreshMessageList(String friendmessageurl) {

        //Getting the username that is stored in SharedPreferences in MainActivity
        sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        String myname = sharedPreferences.getString(Config.USERNAMEKEY, "");

        //callMessageWebService(friendmessageurl, myname);

        MessageTask messageTask = new MessageTask();
        messageTask.execute(friendmessageurl, myname);

    }

    public void callMessageWebService(String friendmessageurl, final String myname) {

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(friendmessageurl)
                .build();

        try {
            Response response = client.newCall(request).execute();
            String jsonData = response.body().string();

            if (response.isSuccessful()) {

                Messages[] message_array = mConversation.getMessages(jsonData);

                mMessagesArrayList = getMessageListValue(message_array);
                //Log.v("MOM", mMessagesArrayList.toString());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMessageAdapter = new MessageAdapter(mMessagesArrayList, MessageActivity.this, myname);
                        mListView.setAdapter(mMessageAdapter);
                        mMessageAdapter.notifyDataSetChanged();
                    }
                });

            }

            response.body().close();

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    private List<Messages> getMessageListValue(Messages[] messages) {

        List<Messages> list = Arrays.asList(messages);
        return list;

    }

    private class MessageTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            callMessageWebService(params[0], params[1]);

            return null;
        }
    }

}
