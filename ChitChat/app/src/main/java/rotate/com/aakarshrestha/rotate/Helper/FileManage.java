package rotate.com.aakarshrestha.rotate.Helper;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class FileManage {


    public static boolean deleteFile(String fileName, Context context) {

        boolean result;

        //File filePath = Environment.getExternalStorageDirectory();

        File filePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File folder = new File(filePath, Config.myAppName);
        File file = new File(folder, fileName + ".jpg");

        boolean success = file.delete();

        if (!success) {
            result = false;
        } else {
            result = true;
        }

        return result;

    }


}


