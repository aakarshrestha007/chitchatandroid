package rotate.com.aakarshrestha.rotate.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.WindowManager;

import rotate.com.aakarshrestha.rotate.R;

import org.json.JSONException;
import org.json.JSONObject;

public class Utility {

    public static boolean isConnectionAvailable(Context context) {

        boolean isOnline;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo !=null && networkInfo.isConnected()) {
            isOnline = true;
        } else {
            isOnline = false;
        }

        return isOnline;

    }

    public static void showAlertMessage(Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme)
                .setTitle("Network Connection")
                .setMessage("Please connect to the internet to continue \nTxtMe.")
                .setPositiveButton("OK", null);
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public static void showAlertMessageForCharacterMoreThan140(Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme)
                .setTitle("")
                .setMessage("Please limit the message to 140 characters.")
                .setPositiveButton("OK", null);
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public static void friendSuccessAlert(Context context, JSONObject jsonObject, String titleName) {

        AlertDialog.Builder builder = null;
        try {
            builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme)
                    .setTitle(titleName)
                    .setMessage(jsonObject.getString("success"))
                    .setPositiveButton("OK", null);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AlertDialog dialog = builder.create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.show();

    }

    public static void friendErrorAlert(Context context, JSONObject jsonObject, String titleName) {

        AlertDialog.Builder builder = null;
        try {
            builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme)
                    .setTitle(titleName)
                    .setMessage(jsonObject.getString("error"))
                    .setPositiveButton("OK", null);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public static String timeFormatter(String theTimeStamp) {

        String timeInAMOrPM;

        String[] theDate = theTimeStamp.split("-");

        String dayTimeCombination = theDate[2];
        String[] dayTimeCombinationSeparator = dayTimeCombination.split(" ");

        String timeCombination = dayTimeCombinationSeparator[1];
        String[] timeCombinationSeparator = timeCombination.split(":");

        String mHour = timeCombinationSeparator[0];

        switch(mHour) {
            case "13":
                timeInAMOrPM = "1"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "14":
                timeInAMOrPM = "2"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "15":
                timeInAMOrPM = "3"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "16":
                timeInAMOrPM = "4"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "17":
                timeInAMOrPM = "5"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "18":
                timeInAMOrPM = "6"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "19":
                timeInAMOrPM = "7"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "20":
                timeInAMOrPM = "8"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "21":
                timeInAMOrPM = "9"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "22":
                timeInAMOrPM = "10"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "23":
                timeInAMOrPM = "11"+":"+timeCombinationSeparator[1]+" PM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            case "24":
                timeInAMOrPM = "00"+":"+timeCombinationSeparator[1]+" AM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
                break;
            default:
                timeInAMOrPM = timeCombinationSeparator[0]+":"+timeCombinationSeparator[1]+" AM"+" "+ theDate[0]+"."+theDate[1]+"."+dayTimeCombinationSeparator[0];
        }


        return timeInAMOrPM;
    }

}


