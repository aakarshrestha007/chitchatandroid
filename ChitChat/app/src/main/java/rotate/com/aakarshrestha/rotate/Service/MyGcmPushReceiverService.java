package rotate.com.aakarshrestha.rotate.Service;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.R;
import rotate.com.aakarshrestha.rotate.UI.FriendRequestActivity;
import rotate.com.aakarshrestha.rotate.UI.MessageActivity;
import rotate.com.aakarshrestha.rotate.UI.ProfileActivity;
import com.google.android.gms.gcm.GcmListenerService;

import java.util.List;
import java.util.Random;

public class MyGcmPushReceiverService extends GcmListenerService {

    public SharedPreferences mSharedPreferences1;
    public SharedPreferences mSharedPreferences2;

    //Notification sound
    public Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

    public SharedPreferences mySharedPref;
    public SharedPreferences friendSharedPref;


    @Override
    public void onMessageReceived(String from, Bundle data) {

        /*
        Send notification to friend when the user sends text message.
         */
        String message = data.getString("messages");
        String sName = data.getString("senderName");

        if (message != null && sName != null) {
            sendNotification(message, sName);

            mSharedPreferences1 = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
            final String myName = mSharedPreferences1.getString(Config.USERNAMEKEY, "Not found!");

        /*
            Sending messages in the intent to the user's message activity
         */
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("GCM_RECEIVED_ACTION");
            broadcastIntent.putExtra("friendname", sName);
            broadcastIntent.putExtra("myname", myName);
            broadcastIntent.putExtra("messageFromService", message);
            sendBroadcast(broadcastIntent);
        }

        /*
        Send notification to potential friend when the user sends friend request
         */
        String newSName = data.getString("sender_name");

        if (newSName != null) {
            sendFriendRequestNotification(newSName);

            mySharedPref = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
            String myName_again = mySharedPref.getString(Config.USERNAMEKEY, "");

            /*
            Sending notification in the intent to the user's FriendRequest activity
            */

            Intent broadcastFriendReqIntent = new Intent();
            broadcastFriendReqIntent.setAction("GCM_FRIEND_REQUEST_RECEIVED_ACTION");
            broadcastFriendReqIntent.putExtra("friendReq_friendname", newSName);
            broadcastFriendReqIntent.putExtra("friendReq_myname", myName_again);
            sendBroadcast(broadcastFriendReqIntent);

        }

        /*
        Send notification to the user to whom the post is made
         */

        String postSName = data.getString("commentor_name");

        if (postSName != null) {
            sendPostCommentNotification(postSName);

            mSharedPreferences1 = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
            final String myName_comment = mSharedPreferences1.getString(Config.USERNAMEKEY, "Not found!");

            /*
                Sending messages in the intent to the user's message activity
             */
            Intent broadcastFriendReqIntent = new Intent();
            broadcastFriendReqIntent.setAction("GCM_POST_COMMENT_RECEIVED_ACTION");
            broadcastFriendReqIntent.putExtra("post_comment_friendname", postSName);
            broadcastFriendReqIntent.putExtra("post_comment_myname", myName_comment);
            sendBroadcast(broadcastFriendReqIntent);
        }

    }

    private void sendNotification(String message, String sName) {

        mSharedPreferences1 = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        final String myName = mSharedPreferences1.getString(Config.USERNAMEKEY, "");

        mSharedPreferences2 = getSharedPreferences("friendGCMPreferences", Context.MODE_PRIVATE);
        final String gcmCode = mSharedPreferences2.getString("fgcmpreferences", "Not Found!");

        Intent intent = new Intent(this, MessageActivity.class);
        intent.putExtra("friendname", sName);
        intent.putExtra("myname", myName);
        intent.putExtra("gcmToken", gcmCode);

//        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
//        taskStackBuilder.addParentStack(NavigationActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //Generate random number
        Random random = new Random();
        int requestCode = random.nextInt(9999 - 1000) + 1000;

//        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        /*
            Build notification
         */
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.logotxtme)
                .setContentTitle("TxtMe - Message")
                .setContentText(sName +": " + message)
                .setSound(notificationSound)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /*
            check if the application is in the foreground. Show notification only when the application is in the background
         */
        if (!isAppInForeground(getApplicationContext())) {
            notificationManager.notify(requestCode, notificationBuilder.build());
        } else {
            notificationManager.cancel(0);
        }

    }

    private void sendFriendRequestNotification(String sender_name) {

        mySharedPref = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        String myName = mySharedPref.getString(Config.USERNAMEKEY, "");

        friendSharedPref = getSharedPreferences("friendReqGCMPreferences", Context.MODE_PRIVATE);
        String theGCMCode = friendSharedPref.getString("freqgcmpreferences", "");

        Intent intent = new Intent(this, FriendRequestActivity.class);
        intent.putExtra("nameOfSender", sender_name);
        intent.putExtra("nameOfFriend", myName);
        intent.putExtra("theGCMCode", theGCMCode);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //Generate random number
        Random random = new Random();
        int requestCode = random.nextInt(9999 - 1000) + 1000;

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.logotxtme)
                .setContentTitle("TxtMe - Friend Request")
                .setContentText("["+sender_name+"]" + " wants to be friends with you.")
                .setSound(notificationSound)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notifManager.notify(requestCode, notifBuilder.build());
    }

    private void sendPostCommentNotification(String postSenderName) {

        mySharedPref = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        String myName = mySharedPref.getString(Config.USERNAMEKEY, "");

        mSharedPreferences2 = getSharedPreferences("friendGCMPreferences", Context.MODE_PRIVATE);
        String postComment_GcmToken = mSharedPreferences2.getString("fgcmpreferences", "Not Found!");

        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("nameOfSender_post", postSenderName);
        intent.putExtra("nameOfFriend_post", myName);
        intent.putExtra("theGCMCode_post", postComment_GcmToken);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //Generate random number
        Random random = new Random();
        int requestCode = random.nextInt(9999 - 1000) + 1000;

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.logotxtme)
                .setContentTitle("TxtMe - Comment!")
                .setContentText("["+postSenderName+"]" + " commented on your profile.")
                .setSound(notificationSound)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notifManager.notify(requestCode, notifBuilder.build());

    }

    /*
        check if the app is in the foreground
     */
    public static boolean isAppInForeground(Context context) {

        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }

        return false;
    }
}


