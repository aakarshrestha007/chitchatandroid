package rotate.com.aakarshrestha.rotate.Helper;

import android.content.Context;
import android.content.SharedPreferences;

public class AutoLogin {

    public static boolean loggedInAlready(Context context) {

        boolean isAlreadyLoggedIn = false;
        SharedPreferences sharedPreferences = context.getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        String u_name = sharedPreferences.getString(Config.USERNAMEKEY, "");

        if (u_name.length() > 0) {
            isAlreadyLoggedIn = true;
        }

        return isAlreadyLoggedIn;

    }

}
