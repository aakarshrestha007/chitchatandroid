package rotate.com.aakarshrestha.rotate.UI;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import rotate.com.aakarshrestha.rotate.Helper.AutoLogin;
import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;
import rotate.com.aakarshrestha.rotate.Service.MyGCMRegistrationIntentService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private TextView mSignUp, mLogin;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private RequestQueue mRequestQueue;
    private StringRequest request;
    public ProgressBar mProgressBar;
    public BroadcastReceiver mBroadcastReceiver;
    public SharedPreferences mSharedPref;
    public SharedPreferences.Editor mSEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        setting background image
         */
        getWindow().setBackgroundDrawableResource(R.drawable.txtmebgnew);

        mProgressBar = (ProgressBar) findViewById(R.id.progressbarID);
        mSignUp = (TextView) findViewById(R.id.lSignupID);
        mLogin = (TextView) findViewById(R.id.loginID);

        //hide the progress bar
        mProgressBar.setVisibility(View.GONE);

        /*
        go to signup activity
         */
        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Signup.class);
                startActivity(intent);

                /*
                Slide the activity
                */
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        /*
        Broadcast Receiver
         */

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //check type of intent filter
                if (intent.getAction().endsWith(MyGCMRegistrationIntentService.REGISTRATION_SUCCESS)) {
                    String token = intent.getStringExtra("token");

                    mSharedPref = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                    mSEditor = mSharedPref.edit();
                    mSEditor.putString(Config.REGISTRATIONTOKEN, token);
                    mSEditor.apply();

                    //Toast.makeText(getApplicationContext(), "My token: " + token, Toast.LENGTH_LONG).show();
                    //Log.w("tokenz", token);
                } else if (intent.getAction().endsWith(MyGCMRegistrationIntentService.REGISTRATION_ERROR)){
                    Toast.makeText(getApplicationContext(), "MYGCMRegistration Error!!!", Toast.LENGTH_LONG).show();
                } else {
                    //to be defined
                }
            }
        };

        //check status of Google Play Service in the device
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultCode) {
            //check types of error
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google play service is not installed or enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device doesn't support Google play service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Start the service
            Intent intent = new Intent(getApplicationContext(), MyGCMRegistrationIntentService.class);
            startService(intent);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
            Broadcast Managers
         */

        //Log.w("NavigationActivity", "onResume");
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(MyGCMRegistrationIntentService.REGISTRATION_SUCCESS));

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(MyGCMRegistrationIntentService.REGISTRATION_ERROR));

        /*
            Auto login if the user has not logged out from the application
         */

        if (AutoLogin.loggedInAlready(getApplicationContext())) {

            Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

             /*
                Slide the activity
             */
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        }

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogTheme)
                                .setTitle("Login");

                        LayoutInflater loginInflater = MainActivity.this.getLayoutInflater();
                        View loginView = loginInflater.inflate(R.layout.login, null);
                        builder.setView(loginView);

                        final EditText mLoginUsername = (EditText) loginView.findViewById(R.id.login_usernameID);
                        final EditText mLoginPassword = (EditText) loginView.findViewById(R.id.login_passwordID);

                        builder.setNegativeButton("Cancel", null)
                                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String mlusername = mLoginUsername.getText().toString().trim();
                                        String mlPassword = mLoginPassword.getText().toString();

                                        /*
                                        Logging into the application
                                        */
                                        loginToApp(mlusername, mlPassword);
                                    }
                                });

                        AlertDialog dialog = builder.create();
                        /*
                        Show keyboard
                         */
                        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                        dialog.show();
                    }
                });

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        /*
            Broadcast Managers
         */
        //Log.w("NavigationActivity", "onPause");
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastReceiver);

    }

    public void loginToApp(final String mlusername, final String mlpassword) {

        /*
        Logging into the application
         */

        mRequestQueue = Volley.newRequestQueue(this);

        /*
        check for the network connection
        */
        if (!Utility.isConnectionAvailable(MainActivity.this)) {
//          Toast.makeText(getApplicationContext(), "Not Connected to the network!", Toast.LENGTH_LONG).show();
            Utility.showAlertMessage(MainActivity.this);

        } else {
            //Toast.makeText(getApplicationContext(), "Connected to the network!", Toast.LENGTH_LONG).show();

            //show progress bar
            mProgressBar.setVisibility(View.VISIBLE);

            request = new StringRequest(Request.Method.POST, Config.URL_USER_CONTROLLER, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.names().get(0).equals("success")) {
                            Toast.makeText(getApplicationContext(), "SUCCESS: " + jsonObject.getString("success"), Toast.LENGTH_SHORT).show();

                            String uname = mlusername;
                            sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                            editor = sharedPreferences.edit();
                            editor.putString(Config.USERNAMEKEY, uname);
                            editor.apply();

                            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                            /*
                            Slide the activity
                             */
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                            //hide progress bar
                            mProgressBar.setVisibility(View.GONE);


                        } else {

                            //hide progress bar
                            mProgressBar.setVisibility(View.INVISIBLE);

                            YoYo.with(Techniques.BounceIn).duration(700).playOn(findViewById(R.id.loginID));
                            Toast.makeText(getApplicationContext(), "ERROR: " + jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //storing the token generated for the device.
                    String gcm_token = mSharedPref.getString(Config.REGISTRATIONTOKEN, "Token not found!");

                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("username", mlusername);
                    hashMap.put("password", mlpassword);
                    hashMap.put("gcm_reg_id", gcm_token);

                    return hashMap;
                }
            };

            mRequestQueue.add(request);
        }

    }

}
