package rotate.com.aakarshrestha.rotate.Helper;

import android.os.AsyncTask;
import android.widget.Toast;

import rotate.com.aakarshrestha.rotate.UI.FriendProfileActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Conversation {

    public boolean checkConverstationStatus(String url, String user_one_name, String user_two_name, String messages, String gcm_token) {

        boolean isMessageSent = false;

        RequestBody mRequestBody = new FormBody.Builder()
                .add("user_one_name", user_one_name)
                .add("user_two_name", user_two_name)
                .add("messages", messages)
                .add("gcm_token", gcm_token)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(mRequestBody)
                .build();

        try {
            Response response = new OkHttpClient().newCall(request).execute();
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            } else {
                isMessageSent = true;
            }

            response.body().close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return isMessageSent;

    }

    public boolean convoCheckInBackground(String url, String user_one_name, String user_two_name, String messages, String gcm_token) {

        ConversationTask conversationTask = new ConversationTask();
        conversationTask.execute(url, user_one_name, user_two_name, messages, gcm_token);

        return true;
    }

    private class ConversationTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            checkConverstationStatus(params[0], params[1], params[2], params[3], params[4]);

            return null;
        }
    }


    public Messages[] getMessages(String jsonData) throws JSONException {


//        JSONArray jsonArray = null;

        JSONArray jsonArray = new JSONArray(jsonData);
        Messages[] messagesArray = new Messages[jsonArray.length()];

        for (int i=0; i<jsonArray.length(); i++) {
            JSONObject jMessage = jsonArray.getJSONObject(i);
            Messages messages = new Messages();
            messages.setMessages(jMessage.getString("message"));
            messages.setSender_name(jMessage.getString("sender_name"));
            messages.setDate(jMessage.getString("createdDate"));

            messagesArray[i] = messages;
        }

//        Log.v("WHITE", messagesArray[0].getMessages());
//        Log.v("WHITE1", messagesArray[0].getSender_name());
//        Log.v("WHITE2", messagesArray[0].getDate());

        return messagesArray;
    }

    public void checkPostCommentOnBackground(String url, String my_name, String friend_name, String comment, String gcmToken) {

        PostCommentTask postCommentTask = new PostCommentTask();
        postCommentTask.execute(url, my_name, friend_name, comment, gcmToken);

    }

    private class PostCommentTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {

            boolean isPostSuccessful = false;

            RequestBody body = new FormBody.Builder()
                    .add("username", params[1])
                    .add("friendname", params[2])
                    .add("comment", params[3])
                    .add("gcm_token", params[4])
                    .build();

            Request request = new Request.Builder()
                    .url(params[0])
                    .post(body)
                    .build();

            try {
                Response response = new OkHttpClient().newCall(request).execute();

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response.body().string());
                } else {
                    isPostSuccessful = true;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return isPostSuccessful;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if (aBoolean) {
                Toast.makeText(FriendProfileActivity.mFriendProfileActivity, "Successfully posted comment!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(FriendProfileActivity.mFriendProfileActivity, "Error posting comment!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public Comment[] getComments(String jsonData) throws JSONException {

        JSONArray friendJsonArray = new JSONArray(jsonData);
        //final String [] commentArray = new String[friendJsonArray.length()];
        Comment [] commentArray = new Comment[friendJsonArray.length()];

        for (int i=0; i<friendJsonArray.length(); i++) {
            JSONObject friendJsonObj = friendJsonArray.getJSONObject(i);
            Comment comment = new Comment();
            String userWhoCommented = friendJsonObj.getString("userone");
            String comments = friendJsonObj.getString("comment");
            String timeStampOfComment = friendJsonObj.getString("createdDate");
            comment.setCommenterName(userWhoCommented);
            comment.setComment(comments);
            comment.setTimeStamp(timeStampOfComment);
            commentArray[i] = comment;
        }

        return commentArray;
    }

}

