package rotate.com.aakarshrestha.rotate.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import rotate.com.aakarshrestha.rotate.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(3500);

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);

                     /*
                        Slide the activity
                    */
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        thread.start();

    }
}

