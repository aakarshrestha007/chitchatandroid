package rotate.com.aakarshrestha.rotate.Helper;

public class FriendRequest {

    String friend_name_req;
    String user_name_req;

    public FriendRequest() {
    }

    public FriendRequest(String friend_name_req, String user_name_req) {
        this.friend_name_req = friend_name_req;
        this.user_name_req = user_name_req;
    }

    public String getFriend_name_req() {
        return friend_name_req;
    }

    public void setFriend_name_req(String friend_name_req) {
        this.friend_name_req = friend_name_req;
    }

    public String getUser_name_req() {
        return user_name_req;
    }

    public void setUser_name_req(String user_name_req) {
        this.user_name_req = user_name_req;
    }

    @Override
    public String toString() {
        return friend_name_req + " " + user_name_req;
    }
}

