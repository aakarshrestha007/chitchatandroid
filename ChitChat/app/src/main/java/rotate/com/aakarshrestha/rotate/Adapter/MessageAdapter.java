package rotate.com.aakarshrestha.rotate.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import rotate.com.aakarshrestha.rotate.Helper.Messages;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends BaseAdapter {

    List<Messages> mMessageList = new ArrayList<>();
    LayoutInflater mInflater;
    Context mContext;
    ViewHolder mHolder;
    private String mMyUserName;

    public MessageAdapter(List<Messages> messageList, Context context, String myusername) {
        mMessageList = messageList;
        mContext = context;
        mMyUserName = myusername;
        mInflater = LayoutInflater.from(mContext);

    }

    @Override
    public int getCount() {
        return mMessageList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMessageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            //show message to right if the sender is you, otherwise to left!!!
            if (mMessageList.get(position).getSender_name().equals(mMyUserName)) {
                convertView = mInflater.inflate(R.layout.row_right, parent, false);
            } else {
                convertView = mInflater.inflate(R.layout.row_left, parent, false);
            }

            mHolder = new ViewHolder();
            mHolder.mMessageText = (TextView) convertView.findViewById(R.id.message);
            mHolder.mTimeText = (TextView) convertView.findViewById(R.id.timeID);
            convertView.setTag(mHolder);

        } else {
            mHolder = (ViewHolder) convertView.getTag();
        }

        mHolder.mMessageText.setText(mMessageList.get(position).getMessages());

        String theTimeStampUserProfile = mMessageList.get(position).getDate();
        mHolder.mTimeText.setText(Utility.timeFormatter(theTimeStampUserProfile));
        mHolder.mTimeText.setTextColor(Color.DKGRAY);
        return convertView;
    }

    static class ViewHolder {
        TextView mMessageText;
        TextView mTimeText;
    }
}


