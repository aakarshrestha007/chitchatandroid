package rotate.com.aakarshrestha.rotate.UI;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import rotate.com.aakarshrestha.rotate.Adapter.GetCommentsAdapter;
import rotate.com.aakarshrestha.rotate.Helper.Comment;
import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.Conversation;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FriendProfileActivity extends AppCompatActivity {

    public OkHttpClient mOkHttpClient = new OkHttpClient();
    public ListView mListView;
    public GetCommentsAdapter mGetCommentAdapter;
    public List<Comment> mCommentList;

    protected ImageView mFriendProfileBackArrow;
    protected ImageButton mFriendWriteComment;
    protected TextView mFriendProfileName;
    protected ImageView mFriendImage;
    protected LinearLayout mLinearLayout2;

    public static FriendProfileActivity mFriendProfileActivity;

    public Conversation conversation = new Conversation();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);

        mFriendProfileActivity = this;

        mFriendProfileBackArrow = (ImageView) findViewById(R.id.friend_profileimage_backarrowID);
        mFriendWriteComment = (ImageButton) findViewById(R.id.friend_writeCommentID);
        mFriendProfileName = (TextView) findViewById(R.id.friendProfileNameID);
        mFriendImage = (ImageView) findViewById(R.id.friend_profileimageID);
        mLinearLayout2 = (LinearLayout) findViewById(R.id.linearLayout2);

        mListView = (ListView) findViewById(R.id.friendListView);
        mCommentList = new ArrayList<>();

    }


    @Override
    protected void onResume() {
        super.onResume();
//        /*
//        Rotate the friend picture
//         */
//        final Animation friendPicRotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
//        mLinearLayout2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mFriendImage.startAnimation(friendPicRotate);
//            }
//        });

        Intent friendIntent = getIntent();
        String my_name = friendIntent.getStringExtra("mynameYo");
        final String friend_name = friendIntent.getStringExtra("friendNameYo");

        /*
            Comment list
         */

        getComments(friend_name);


        //animation
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                YoYo.with(Techniques.FlipOutX).duration(1000).playOn(mFriendImage);
//                YoYo.with(Techniques.FlipInX).duration(2000).playOn(mFriendImage);
//            }
//        });

        /*
            Get the friend profile image from the server
         */
        getFriendImage(friend_name);


        /*
            Setting the friend name
         */
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mFriendProfileName.setText(friend_name);
            }
        });

        mFriendProfileBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);

            }
        });

        mFriendWriteComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectionAvailable(FriendProfileActivity.this)) {
                    Utility.showAlertMessage(FriendProfileActivity.this);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(FriendProfileActivity.this, R.style.AlertDialogTheme);
                            builder.setTitle("Write comment");

                            LayoutInflater inflater = FriendProfileActivity.this.getLayoutInflater();
                            View layout = inflater.inflate(R.layout.dialog, null);
                            builder.setView(layout);

                            final EditText input = (EditText)layout.findViewById(R.id.dialogID);
                            input.setBackgroundColor(Color.WHITE);
                            input.setTextColor(Color.BLACK);

                            builder.setNegativeButton("Cancel", null)
                                    .setPositiveButton("Post", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            Intent friendIntent = getIntent();
                                            String my_name = friendIntent.getStringExtra("mynameYo");
                                            final String friend_name = friendIntent.getStringExtra("friendNameYo");
                                            final String friend_gcm_token = friendIntent.getStringExtra("friendGcmToken");

                                            String comment = input.getText().toString().trim();

                                            if (!comment.isEmpty()) {
                                            /*
                                                Post the comment in the background
                                            */
                                                conversation.checkPostCommentOnBackground(Config.URL_POST_COMMENT, my_name, friend_name, comment.replace("'", "\''"), friend_gcm_token);

                                            /*
                                            Comment list
                                             */
                                                getComments(friend_name);

                                            } else {
                                                Toast.makeText(getApplicationContext(), "There is no comment to post!", Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    });


                            AlertDialog dialog = builder.create();
                            /*
                            show keyboard
                             */
                            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                            dialog.show();
                        }
                    });
                }

            }
        });

    }

    private List<Comment> getCommentListValue(Comment[] commentsArray) {

        List<Comment> list = Arrays.asList(commentsArray);
        return list;

    }

    public void getComments(String friendname) {
        refreshPostCommentList(friendname);
    }

    public void refreshPostCommentList(String friendName) {

        CommentTask commentTask = new CommentTask();
        commentTask.execute(friendName);

    }

    private class CommentTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            callPostCommentWebService(params[0]);

            return null;
        }
    }

    public void callPostCommentWebService(String friendname) {

        Request request = new Request.Builder()
                .url(Config.URL_GET_POST_COMMENT + friendname + Config.token)
                .build();

        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String friendResponse = response.body().string();

                    try {

                        mCommentList = getCommentListValue(conversation.getComments(friendResponse));

                        if (!mCommentList.isEmpty()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mGetCommentAdapter = new GetCommentsAdapter(mCommentList, FriendProfileActivity.this);
                                    mListView.setAdapter(mGetCommentAdapter);
                                    mGetCommentAdapter.notifyDataSetChanged();
                                }
                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    response.body().close();
                }

            }
        });

    }

    public void getFriendImage(String friend_name) {

        Request request = new Request.Builder()
                .url(Config.URL_USER_PROFILE + friend_name + Config.token)
                .build();

        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String friendImagePath = null;

                if (response.isSuccessful()) {
                    String mResponse = response.body().string();

                    try {
                        JSONArray jsonArray = new JSONArray(mResponse);

                        for (int i=0; i<jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            friendImagePath = jsonObject.getString("image_path");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                final String copyOfFriendImagePath = friendImagePath;
                final String myFriendImagePath = Config.URL_BASIC + friendImagePath;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //set a profile image otherwise set default image
                        if (myFriendImagePath.equals("null") || copyOfFriendImagePath.equals("null")) {
                            Picasso.with(getApplicationContext()).load(R.drawable.dummy).into(mFriendImage);
                        } else {
                            Picasso.with(getApplicationContext()).load(myFriendImagePath).into(mFriendImage);

                            /*
                            Display the profile image in the alert dialog
                             */
                            mFriendImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(FriendProfileActivity.this, R.style.AlertDialogTheme)
                                                    .setCancelable(true);

                                            LayoutInflater inflater = LayoutInflater.from(FriendProfileActivity.this);
                                            View view = inflater.inflate(R.layout.alert_profile_image, null);
                                            builder.setView(view);

                                            ImageView imageView = (ImageView) view.findViewById(R.id.alertProfileImageID);
                                            Picasso.with(FriendProfileActivity.this).load(myFriendImagePath).into(imageView);

                                            builder.setNeutralButton("Close", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });

                                            AlertDialog dialog = builder.create();
                                            dialog.show();
                                        }
                                    });
                                }
                            });

                        }

                    }
                });

            }
        });

    }
}

