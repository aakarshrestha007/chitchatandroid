package rotate.com.aakarshrestha.rotate.Adapter;

import android.content.Context;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CustomAdapter extends BaseAdapter{

    private Context mContext;
    private List<String> myList = new ArrayList<>();
    public File[] listFiles;

    public CustomAdapter(Context context) {
        mContext = context;
        getSDCardImages();

    }

    public void getSDCardImages() {

//        File sd = Environment.getExternalStorageDirectory();
//        File folder = new File(sd, Config.myAppName);

        final File sd = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        final File folder = new File(sd, Config.myAppName);


        if (folder.isDirectory()) {

            listFiles = folder.listFiles();
            //Log.v("PATH", listFiles.toString());

            for (int i=0; i<listFiles.length; i++) {
                String path = listFiles[i].getAbsolutePath();
                //  String fileName = path.substring(path.lastIndexOf('/') + 1);
                //  Log.v("SEA", String.valueOf(listFiles.length));
                //  Log.v("SKY", path);
                myList.add(path);

            }

        }

    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.friend_list_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String fileNamePath = myList.get(position);
        holder.mTextView.setText(getUsernameOnly(fileNamePath));

        File theFile = new File(myList.get(position));

        if (theFile.length() == 0) {
            Picasso.with(mContext).load(R.drawable.dummy).resize(120, 120).into(holder.mImageView);
        } else {
            Picasso.with(mContext).load(new File(myList.get(position))).resize(120, 120).into(holder.mImageView);
        }

        notifyDataSetChanged();

        return convertView;
    }

    private static class ViewHolder {

        public ImageView mImageView;
        public TextView mTextView;

        public ViewHolder(View v) {
            mImageView = (ImageView) v.findViewById(R.id.friendImageID);
            mTextView = (TextView) v.findViewById(R.id.friendNameID);
        }
    }

    private String getUsernameOnly(String fileNamePath) {
        String imageName = fileNamePath.substring(fileNamePath.lastIndexOf('/') + 1);
        String username = imageName.substring(0, imageName.lastIndexOf('.'));

        return username;
    }


}

