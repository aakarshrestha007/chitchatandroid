package rotate.com.aakarshrestha.rotate.UI;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import rotate.com.aakarshrestha.rotate.Adapter.CustomAdapter;
import rotate.com.aakarshrestha.rotate.Helper.BuildVersionCheck;
import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.Users;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;
import rotate.com.aakarshrestha.rotate.Service.DownloadFriendIntentService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected List<Users> mUsersList;
    protected List<Users> mUsersListOne;
    private TextView mProfileUsername;
    private NavigationView navigation_view;

    private GridView mGridView;

    //checking the build version
    BuildVersionCheck mBuildVersionCheck;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private OkHttpClient client = new OkHttpClient();

    private CustomAdapter mCustomAdapter;

    public String friendRegistrationToken;

    public SharedPreferences.Editor friendEditor;

    public SharedPreferences friendGCMPreferences;

    public static NavigationActivity mNavigationActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        mNavigationActivity = this;

        /*
            check the buildversion
         */
        mBuildVersionCheck = new BuildVersionCheck();
        mBuildVersionCheck.checkBuildVersion();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Chat With Friends");
        setSupportActionBar(toolbar);

        //Getting the username that is stored in SharedPreferences in MainActivity
        sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        final String u_name = sharedPreferences.getString(Config.USERNAMEKEY, "");

        setNavigationHeader(); // setting the nav_header_navigation.xml here
        setUserProfile(u_name);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Navigate to FindFriendActivity to search friends - it is a shortcut way
                Intent intent = new Intent(NavigationActivity.this, FindFriendActivity.class);
                intent.putExtra("friend_name_from_comment", "");
                startActivity(intent);

                /*
                Slide the activity
                 */
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //important here
        navigation_view.setNavigationItemSelectedListener(this);

        /*
            GridView and Lists defined here
         */
        mGridView = (GridView) findViewById(R.id.listView2);
        mUsersList = new ArrayList<>();
        mUsersListOne = new ArrayList<>();

    }

    public NavigationActivity getNavigationActivity() {
        return mNavigationActivity;
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*
            get the list of added friends
         */
        Intent intentService = new Intent(getApplicationContext(), DownloadFriendIntentService.class);
        startService(intentService);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        /*
        This method prevents the app being killed when back button is clicked.
         */

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }

    /*
        Set Navigation header by using Layout Inflater.
     */

    public void setNavigationHeader(){

        navigation_view = (NavigationView) findViewById(R.id.nav_view);

        View view = LayoutInflater.from(this).inflate(R.layout.nav_header_navigation, null);
        navigation_view.addHeaderView(view);

        mProfileUsername = (TextView) view.findViewById(R.id.profileUsernameID);
    }

      /*
          Set User Profile Information in Navigation Bar.
       */

    public  void  setUserProfile(String value) {
        mProfileUsername.setText("Welcome, " + value);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {


            //reload the listview
            getListView();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {

            //Toast.makeText(getApplicationContext(), item.getTitle().toString(), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(NavigationActivity.this, ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

            /*
            Slide the activity
             */
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        } else if (id == R.id.nav_findfriends) {

            //Getting the username that is stored in SharedPreferences in MainActivity
            String u_name = sharedPreferences.getString(Config.USERNAMEKEY, "");
            //Toast.makeText(getApplicationContext(), "Coming for: " + u_name.toString(), Toast.LENGTH_LONG).show();
            SharedPreferences sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            editor.putString(Config.USERNAMEKEY, u_name);
            editor.apply();

            Intent intent = new Intent(NavigationActivity.this, FindFriendActivity.class);
            intent.putExtra("friend_name_from_comment", "");
            startActivity(intent);
            /*
            Slide the activity
             */
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        } else if (id == R.id.nav_friend_request_status) {

            Intent intent = new Intent(NavigationActivity.this, FriendRequestActivity.class);
            startActivity(intent);
            /*
            Slide the activity
             */
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.logout) {

            AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this, R.style.AlertDialogTheme)
                    .setTitle("Logging out...")
                    .setMessage("Are you sure want to log out?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            logOut();
                        }
                    })
                    .setNegativeButton("No", null);

            AlertDialog dialog = builder.create();
            dialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getListView() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mCustomAdapter = new CustomAdapter(getApplicationContext());
                mGridView.setAdapter(mCustomAdapter);
                mGridView.setScrollingCacheEnabled(false);
                mCustomAdapter.notifyDataSetChanged();

                //animation
                YoYo.with(Techniques.FadeIn).duration(500).playOn(mGridView);

                mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, final View view, int position, final long id) {

                        if (!Utility.isConnectionAvailable(NavigationActivity.this)) {
                            Utility.showAlertMessage(NavigationActivity.this);
                        } else {

                            String name = parent.getItemAtPosition(position).toString();
                            final String friendName = getUsernameOnly(name);
                            final String u_name = sharedPreferences.getString(Config.USERNAMEKEY, "");
                            //Toast.makeText(getApplicationContext(), getUsernameOnly(name), Toast.LENGTH_SHORT).show();

                            //make a webservice call to get the gcm registration id of the friend.
                            OkHttpClient client = new OkHttpClient();

                            Request request = new Request.Builder()
                                    .url(Config.URL_USER_PROFILE + friendName + Config.token)
                                    .build();

                            client.newCall(request).enqueue(new Callback() {
                                @Override
                                public void onFailure(Call call, IOException e) {

                                }

                                @Override
                                public void onResponse(Call call, Response response) throws IOException {

                                    if (response.isSuccessful()) {

                                        friendRegistrationToken = response.body().string();

                                        try {
                                            JSONArray jsonArray = new JSONArray(friendRegistrationToken);

                                            for (int i=0; i<jsonArray.length(); i++) {
                                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                                final String gcm_token = jsonObject.getString("gcm_regid").trim();

                                                friendGCMPreferences = getSharedPreferences("friendGCMPreferences", Context.MODE_PRIVATE);
                                                friendEditor = friendGCMPreferences.edit();
                                                friendEditor.putString("fgcmpreferences", gcm_token);
                                                friendEditor.apply();

                                                /*
                                                    if the token is not empty, navigate to MessageActivity otherwise show an error.
                                                 */
                                                if (!gcm_token.isEmpty()) {
                                                    Intent intent = new Intent(NavigationActivity.this, MessageActivity.class);
                                                    intent.putExtra("friendname", friendName);
                                                    intent.putExtra("myname", u_name);
                                                    intent.putExtra("gcmToken", gcm_token);
                                                    startActivity(intent);

                                                    /*
                                                    Slide the activity
                                                    */
                                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                                                } else {

                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Snackbar.make(view, "Something is empty!!!", Snackbar.LENGTH_LONG).show();
                                                            //Toast.makeText(getApplicationContext(), "Something is empty!", Toast.LENGTH_LONG).show();
                                                        }
                                                    });
                                                }

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }

                                }
                            });

                        }

                    }
                });
            }
        });

    }


    private String getUsernameOnly(String fileNamePath) {
        String imageName = fileNamePath.substring(fileNamePath.lastIndexOf('/') + 1);
        String username = imageName.substring(0, imageName.lastIndexOf('.'));

        return username;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        boolean isMounted;
        String state = Environment.getExternalStorageState();

        if (state.equals(Environment.MEDIA_MOUNTED)) {
            isMounted = true;
        } else {
            isMounted = false;
        }

        return isMounted;
    }

    public void storeImage(final Bitmap bitmap, final File myFile) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        final byte[] byteArray = bos.toByteArray();

        new Thread(new Runnable() {
            @Override
            public void run() {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(myFile);
                    outputStream.write(byteArray);
                    outputStream.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void logOut() {

        /*
        Stop downloading friends images in the background
         */

        if (new Intent(getApplicationContext(), DownloadFriendIntentService.class) != null) {
            stopService(new Intent(getApplicationContext(), DownloadFriendIntentService.class));
        }

        //stopService(new Intent(getApplicationContext(), DownloadFriendIntentService.class));

        File folderpath = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File folder = new File(folderpath, Config.myAppName);

        if (folder.exists() && folder.isDirectory()) {

            while (!clearAllImagesFromCache()) {

                try {
                    Thread.sleep(900);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            goBackToLogin();

        } else {
            goBackToLogin();
        }

    }

    public void goBackToLogin() {

        //Getting the username stored in the shared preferences
        String gcm_username = sharedPreferences.getString(Config.USERNAMEKEY, "");
        //Update the gcm registration id when user is logging out of the app.
        UpdateGcmIDTask updateGcmIDTask = new UpdateGcmIDTask();
        updateGcmIDTask.execute(gcm_username);

        while (!removeUsernameFromSharedPreference()) {
            removeUsernameFromSharedPreference();
        }

        Intent intent = new Intent(NavigationActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

         /*
        Slide the activity
        */
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public boolean removeUsernameFromSharedPreference() {

        boolean isUsernameRemovedFromSharedPreference = false;

        sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES,  Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(Config.USERNAMEKEY);
        editor.apply();

        if (sharedPreferences.getString(Config.USERNAMEKEY, "").length() == 0) {
            isUsernameRemovedFromSharedPreference = true;
        }

        return isUsernameRemovedFromSharedPreference;

    }

    public boolean clearAllImagesFromCache() {

        boolean isImageFileEmpty;

        //final File file = new File(Environment.getExternalStorageDirectory(), Config.myAppName);

        final File folderpath = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        final File file = new File(folderpath, Config.myAppName);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                if (file.isDirectory()) {

                    String[] fileArray = file.list();
                    for (String aFileArray : fileArray) {
                        new File(file, aFileArray).delete();
                    }

                }

            }
        });

        thread.start();

        if (file.list().length < 1) {
            isImageFileEmpty = true;
        } else {
            isImageFileEmpty = false;
        }

        return isImageFileEmpty;

    }

    public int countFiles() {
        //final File folder = new File(Environment.getExternalStorageDirectory(), Config.myAppName);

        final File folderpath = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        final File folder = new File(folderpath, Config.myAppName);

        File[] files = folder.listFiles();
        return files.length;

    }

    /*
    Update the user's gcm registration id when user logs out from the application.
     */

    private class UpdateGcmIDTask extends AsyncTask<String , Void, Void> {


        @Override
        protected Void doInBackground(String... params) {

            RequestBody body = new FormBody.Builder()
                    .add("username", params[0])
                    .build();

            Request request = new Request.Builder()
                    .url(Config.URL_UPDATE_USER_GCM)
                    .post(body)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if (response.isSuccessful()) {

                        String mResponse = response.body().string();

                        try {
                            JSONObject jsonObject = new JSONObject(mResponse);

                            if (jsonObject.names().get(0).equals("success")) {
                                // Successfully update the gcm id of the user
                            } else {
                                // Failed to update the gcm id of the user
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }
            });


            return null;
        }
    }

}


