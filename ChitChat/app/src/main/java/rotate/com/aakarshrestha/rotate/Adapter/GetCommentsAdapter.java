package rotate.com.aakarshrestha.rotate.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import rotate.com.aakarshrestha.rotate.Helper.Comment;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;
import rotate.com.aakarshrestha.rotate.UI.FindFriendActivity;

import java.util.ArrayList;
import java.util.List;

public class GetCommentsAdapter extends BaseAdapter{

    List<Comment> mCommentList = new ArrayList<>();
    Context mContext;
    LayoutInflater mInflater;
    ViewHolder mViewHolder;

    public GetCommentsAdapter(List<Comment> commentList, Context context) {
        mCommentList = commentList;
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mCommentList.size();
    }

    @Override
    public Object getItem(int position) {
        return mCommentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.friend_profile_listview_item, parent, false);
            mViewHolder = new ViewHolder();
            mViewHolder.mCommenterName = (TextView) convertView.findViewById(R.id.commenter_nameID);
            mViewHolder.mCommentText = (TextView) convertView.findViewById(R.id.commentID);
            mViewHolder.mCommentTimeStamp = (TextView) convertView.findViewById(R.id.timestampID);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        final Comment comment = mCommentList.get(position);

        mViewHolder.mCommenterName.setText(comment.getCommenterName());
        mViewHolder.mCommentText.setText(comment.getComment());

        /*
        Click on the commenter's name and navigate to findfriend activity
         */

        mViewHolder.mCommenterName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String friend_name_from_comment = comment.getCommenterName();

                Intent intent = new Intent(mContext, FindFriendActivity.class);
                intent.putExtra("friend_name_from_comment", friend_name_from_comment);
                mContext.startActivity(intent);
            }
        });

        String theTimeStampFriendProfile = comment.getTimeStamp();
        mViewHolder.mCommentTimeStamp.setText(Utility.timeFormatter(theTimeStampFriendProfile));
        mViewHolder.mCommentText.setTextColor(Color.BLACK);

        return convertView;
    }

    static class ViewHolder {

        public TextView mCommenterName;
        public TextView mCommentText;
        public TextView mCommentTimeStamp;

    }

}

