package rotate.com.aakarshrestha.rotate.Helper;

public class FriendProfile {

    private String friendName;
    private int id;

    public FriendProfile() {
    }

    public FriendProfile(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return friendName;
    }
}


