package rotate.com.aakarshrestha.rotate.Helper;

public class Messages {

    String mSender_name;
    String mMessages;
    String mDate;

    public Messages() {
    }

    public Messages(String sender_name, String messages, String date) {
        mSender_name = sender_name;
        mMessages = messages;
        mDate = date;
    }

    public String getSender_name() {
        return mSender_name;
    }

    public void setSender_name(String sender_name) {
        mSender_name = sender_name;
    }

    public String getMessages() {
        return mMessages;
    }

    public void setMessages(String messages) {
        mMessages = messages;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    @Override
    public String toString() {
        return mMessages + " \n" + mSender_name + " \n" + mDate;
    }
}


