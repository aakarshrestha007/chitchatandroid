package rotate.com.aakarshrestha.rotate.Service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.Users;
import rotate.com.aakarshrestha.rotate.UI.NavigationActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DownloadFriendIntentService extends IntentService {

    /*
    Constructor
     */
    public DownloadFriendIntentService() {
        super("DownloadFriendIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        synchronized (this) {

            SharedPreferences sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
            String u_name = sharedPreferences.getString(Config.USERNAMEKEY, "");

            List<Users> mUsersList = new ArrayList<>();
            List<Users> mUsersListOne = new ArrayList<>();

            /*
            Download all the friends and their images
             */
            DownloadImageTaskNow(u_name, mUsersList, mUsersListOne);

            /*
            Show the friends in navigation activity and Friend Request Activity
             */
            NavigationActivity.mNavigationActivity.getNavigationActivity().getListView();

            //Log.v("HELLO", "IntentService is working!");
        }

    }

    public void DownloadImageTaskNow(final String usernameValue, final List<Users> mUsersList, final List<Users> mUsersListOne) {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(Config.URL_FRIENDSHIP_TAG + usernameValue + Config.token)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String mResponse = response.body().string();

                    try {
                        JSONArray jsonArray = new JSONArray(mResponse);

                        for (int i=0; i<jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String user_one_name = jsonObject.getString("user_one");
                            String user_two_name = jsonObject.getString("user_two");
                            String userone_image_path = jsonObject.getString("user_one_image_path");
                            String usertwo_image_path = jsonObject.getString("user_two_image_path");

                            Users users = new Users();
                            users.setUserOne(user_one_name);
                            users.setUserTwo(user_two_name);
                            users.setUserOneImagePath(userone_image_path);
                            users.setUserTwoImagePath(usertwo_image_path);
                            mUsersList.add(users);

                            //publishProgress(mUsersList.size());

                            if (usernameValue.equals(user_one_name)) {

                                for (int j = 0; j < mUsersList.size(); j++) {

                                    Users userTwoImageObject = mUsersList.get(j);

                                    //if the image path is null, only create the file but don't write on it
                                    if (userTwoImageObject.getUserTwoImagePath().isEmpty()) {
                                        if (NavigationActivity.mNavigationActivity.getNavigationActivity().isExternalStorageWritable()) {

                                            //File sdCardPath = Environment.getExternalStorageDirectory();
                                            File sdCardPath = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                                            File folder = new File(sdCardPath, Config.myAppName);

                                            if (!folder.exists()) {
                                                if (!folder.mkdir()) {
                                                    folder.mkdir();
                                                }
                                            } else {
                                                final File myFile = new File(folder, userTwoImageObject.getUserTwo() + ".jpg");

                                                if (!myFile.exists()) {
                                                    myFile.createNewFile();
                                                }

                                            }

                                        }
                                    } else {
                                        URL mUrl = new URL(Config.URL_BASIC + userTwoImageObject.getUserTwoImagePath());

                                        InputStream inputStream = new BufferedInputStream(mUrl.openStream(), 1024);


                                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                                        if (NavigationActivity.mNavigationActivity.getNavigationActivity().isExternalStorageWritable()) {

                                            //File sdCardPath = Environment.getExternalStorageDirectory();
                                            File sdCardPath = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                                            File folder = new File(sdCardPath, Config.myAppName);

                                            if (!folder.exists()) {
                                                if (!folder.mkdir()) {
                                                    folder.mkdir();
                                                }
                                            } else {
                                                final File myFile = new File(folder, userTwoImageObject.getUserTwo() + ".jpg");

                                                if (!myFile.exists()) {
                                                    myFile.createNewFile();

                                                    NavigationActivity.mNavigationActivity.getNavigationActivity().storeImage(bitmap, myFile);
                                                }
//                                                else {
//
//                                                    if (myFile.length() == 0) {
//                                                        NavigationActivity.mNavigationActivity.getNavigationActivity().storeImage(bitmap, myFile);
//                                                    }
//
//                                                }

                                            }

                                        }
                                    }

                                }

                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

        });

        /*
            When the user accepts the friend request
            second webservice friendship_tag_one is called
         */

        Request request_one = new Request.Builder()
                .url(Config.URL_FRIENDSHIP_TAG_ONE + usernameValue + Config.token)
                .build();


        Call call_one = client.newCall(request_one);
        call_one.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String mResponse_one = response.body().string();

                    try {
                        JSONArray jsonArray_one = new JSONArray(mResponse_one);

                        for (int i=0; i<jsonArray_one.length(); i++) {
                            JSONObject jsonObject = jsonArray_one.getJSONObject(i);
                            String user_one_name = jsonObject.getString("user_one");
                            String user_two_name = jsonObject.getString("user_two");
                            String userone_image_path = jsonObject.getString("user_one_image_path");
                            String usertwo_image_path = jsonObject.getString("user_two_image_path");

                            Users oneuser = new Users();
                            oneuser.setUserOne(user_one_name);
                            oneuser.setUserTwo(user_two_name);
                            oneuser.setUserOneImagePath(userone_image_path);
                            oneuser.setUserTwoImagePath(usertwo_image_path);
                            mUsersListOne.add(oneuser);

                            //publishProgress(mUsersListOne.size());

                            if (usernameValue.equals(user_two_name)) {

                                for (int k = 0; k < mUsersListOne.size(); k++) {

                                    Users usersOne = mUsersListOne.get(k);

                                    //if the image path is null, only create the file but don't write on it
                                    if (usersOne.getUserOneImagePath().isEmpty()) {
                                        if (NavigationActivity.mNavigationActivity.getNavigationActivity().isExternalStorageWritable()) {

                                            //File sd = Environment.getExternalStorageDirectory();
                                            File sd = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                                            File folder = new File(sd, Config.myAppName);

                                            if (!folder.exists()) {
                                                if (!folder.mkdir()) {
                                                    folder.mkdir();
                                                }
                                            } else {
                                                final File myFile = new File(folder, usersOne.getUserOne() + ".jpg");

                                                if (!myFile.exists()) {
                                                    myFile.createNewFile();
                                                }

                                            }

                                        }
                                    } else {
                                        URL mUrl = new URL(Config.URL_BASIC + usersOne.getUserOneImagePath());
                                        InputStream inputStream = new BufferedInputStream(mUrl.openStream(), 1024);

                                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                                        if (NavigationActivity.mNavigationActivity.getNavigationActivity().isExternalStorageWritable()) {

                                            //File sd = Environment.getExternalStorageDirectory();
                                            File sd = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                                            File folder = new File(sd, Config.myAppName);

                                            if (!folder.exists()) {
                                                if (!folder.mkdir()) {
                                                    folder.mkdir();
                                                }
                                            } else {
                                                final File myFile = new File(folder, usersOne.getUserOne() + ".jpg");

                                                if (!myFile.exists()) {
                                                    myFile.createNewFile();
                                                    NavigationActivity.mNavigationActivity.storeImage(bitmap, myFile);
                                                }
//                                                else {
//                                                    if (myFile.length() == 0) {
//                                                        NavigationActivity.mNavigationActivity.getNavigationActivity().storeImage(bitmap, myFile);
//                                                    }
//                                                }

                                            }

                                        }
                                    }

                                }

                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

    }

}


