package rotate.com.aakarshrestha.rotate.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UsersInfo {

    public UserProfile[] getCurrentUserProfile(String jsonData) throws JSONException {

        JSONArray jsonArray = null;
        jsonArray = new JSONArray(jsonData);

        UserProfile[] userProfiles = new UserProfile[jsonArray.length()];

        for (int i=0; i<jsonArray.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonArray.getJSONObject(i);
                UserProfile userProfile = new UserProfile();
                userProfile.setUsername(jsonObject.getString("username"));
                userProfile.setEmail(jsonObject.getString("email").toString());
                userProfile.setDate(jsonObject.getString("createdDate").toString());

                userProfiles[i] = userProfile;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return userProfiles;

    }

}
