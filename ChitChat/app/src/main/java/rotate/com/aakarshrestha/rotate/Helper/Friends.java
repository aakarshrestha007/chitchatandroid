package rotate.com.aakarshrestha.rotate.Helper;

public class Friends {

    public String mFriendNameString;
    public String mFriendImageString;

    public Friends() {
    }

    public Friends(String friendNameString, String friendImageString) {
        mFriendNameString = friendNameString;
        mFriendImageString = friendImageString;
    }

    public String getFriendNameString() {
        return mFriendNameString;
    }

    public void setFriendNameString(String friendNameString) {
        mFriendNameString = friendNameString;
    }

    public String getFriendImageString() {
        return mFriendImageString;
    }

    public void setFriendImageString(String friendImageString) {
        mFriendImageString = friendImageString;
    }

    @Override
    public String toString() {
        return mFriendNameString;
    }
}


