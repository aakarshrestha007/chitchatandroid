package rotate.com.aakarshrestha.rotate.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.Friends;
import rotate.com.aakarshrestha.rotate.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FriendsListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Friends> mFriendsList;

    public FriendsListAdapter(Context context, List<Friends> friendsList) {
        mContext = context;
        mFriendsList = friendsList;
    }

    @Override
    public int getCount() {
        return mFriendsList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFriendsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.friend_list_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Friends friends  = mFriendsList.get(position);

        holder.mFriendNameText.setText(friends.getFriendNameString());
        holder.mFriendNameText.setTextColor(Color.BLACK);

        if (friends.getFriendImageString().equals(Config.URL_BASIC+"null")) {
            Picasso.with(mContext).load(android.R.drawable.sym_def_app_icon).into(holder.mFriendImage);
        } else {
            Picasso.with(mContext).load(friends.getFriendImageString()).into(holder.mFriendImage);
        }

        return convertView;
    }

    private class ViewHolder {

        public TextView mFriendNameText;
        public ImageView mFriendImage;

        public ViewHolder(View view) {
            mFriendNameText = (TextView) view.findViewById(R.id.friendNameID);
            mFriendImage = (ImageView) view.findViewById(R.id.friendImageID);
        }
    }
}
