package rotate.com.aakarshrestha.rotate.Service;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import rotate.com.aakarshrestha.rotate.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class MyGCMRegistrationIntentService extends IntentService {

    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";

    public MyGCMRegistrationIntentService() {
        super("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        registerGCM();
    }

    private void registerGCM() {

        Intent registrationComplete = null;
        String token = null;

        try {
            InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            //Log.v("GCMRegIntentService", "token:" + token);

            registrationComplete = new Intent(REGISTRATION_SUCCESS);
            registrationComplete.putExtra("token", token);


        } catch (IOException e) {
            e.printStackTrace();
            //Log.v("GCNRegIntentService", "Registration Error");
            registrationComplete = new Intent(REGISTRATION_ERROR);
        }

        //send the broadcast
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(registrationComplete);

    }
}


