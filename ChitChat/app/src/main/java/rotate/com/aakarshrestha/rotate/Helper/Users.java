package rotate.com.aakarshrestha.rotate.Helper;

public class Users {

    public String userOne;
    public String userTwo;
    public String userOneImagePath;
    public String userTwoImagePath;
    public String userOneImageName;
    public String userTwoImageName;
    public String imagePath;
    public String imageName;

    public Users() {
    }

    public Users(String userOne, String userTwo, String userOneImagePath, String userTwoImagePath) {
        this.userOne = userOne;
        this.userTwo = userTwo;
        this.userOneImagePath = userOneImagePath;
        this.userTwoImagePath = userTwoImagePath;
    }

    public String getUserOne() {
        return userOne;
    }

    public void setUserOne(String userOne) {
        this.userOne = userOne;
    }

    public String getUserOneImagePath() {
        return userOneImagePath;
    }

    public void setUserOneImagePath(String userOneImagePath) {
        this.userOneImagePath = userOneImagePath;
    }

    public String getUserTwoImagePath() {
        return userTwoImagePath;
    }

    public void setUserTwoImagePath(String userTwoImagePath) {
        this.userTwoImagePath = userTwoImagePath;
    }

    public String getUserOneImageName() {
        return userOneImageName;
    }

    public void setUserOneImageName(String userOneImageName) {
        this.userOneImageName = userOneImageName;
    }

    public String getUserTwoImageName() {
        return userTwoImageName;
    }

    public void setUserTwoImageName(String userTwoImageName) {
        this.userTwoImageName = userTwoImageName;
    }

//    public String getImageName() {
//        return imageName;
//    }
//
//    public void setImageName(String imageName) {
//        this.imageName = imageName;
//    }



    public String getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(String userTwo) {
        this.userTwo = userTwo;
    }

//    public String getImagePath() {
//        return imagePath;
//    }
//
//    public void setImagePath(String imagePath) {
//        this.imagePath = imagePath;
//    }

    @Override
    public String toString() {
        return userOne + "\n" + userTwo + "\n" + userOneImagePath + "\n" + userTwoImagePath;
    }
}

