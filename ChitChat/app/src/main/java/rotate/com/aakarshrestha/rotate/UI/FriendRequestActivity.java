package rotate.com.aakarshrestha.rotate.UI;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import rotate.com.aakarshrestha.rotate.Adapter.FriendRequestAdapter;
import rotate.com.aakarshrestha.rotate.Helper.AutoLogin;
import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.FriendRequest;
import rotate.com.aakarshrestha.rotate.Helper.Utility;
import rotate.com.aakarshrestha.rotate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FriendRequestActivity extends AppCompatActivity {

    public ImageView backArrow;
    public ProgressBar mFriendReqProgressBar;

    public SharedPreferences sharedPreferences;

    public ListView mListViewFriendRequest;
    public List<FriendRequest> mFriendRequestList;
    public FriendRequestAdapter mFriendRequestAdapter;

    public FriendRequest mFriendRequest;

    public OkHttpClient mOkHttpClient;

    Activity thisActivity=(Activity)this;

    public IntentFilter mFriendReqIntentFilter;

    BroadcastReceiver mFriendReqBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String friendName = intent.getExtras().getString("friendReq_friendname");
            String myName = intent.getExtras().getString("friendReq_myname");

            if (friendName != null && myName != null) {

                if (AutoLogin.loggedInAlready(getApplicationContext())) {
                    downloadRequest(myName);
                }
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_request);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("");
        }
        setSupportActionBar(toolbar);

        backArrow = (ImageView) findViewById(R.id.backArrowIDimage_friend_req);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FriendRequestActivity.this, ProfileActivity.class);
                startActivity(intent);

                /*
                Slide the activity
                */
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        mFriendReqProgressBar = (ProgressBar) findViewById(R.id.friendrequest_progressbarID);

        mFriendRequestList = new ArrayList<>();

        mOkHttpClient = new OkHttpClient();

        sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);

        /*
        Add action of the broadcast receiver
         */
        mFriendReqIntentFilter = new IntentFilter();
        mFriendReqIntentFilter.addAction("GCM_FRIEND_REQUEST_RECEIVED_ACTION");
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
        Hide the progress bar
         */
        mFriendReqProgressBar.setVisibility(View.GONE);

        mListViewFriendRequest = (ListView) findViewById(R.id.listView_friend_req);
        String username_f_req = sharedPreferences.getString(Config.USERNAMEKEY, "Not found!");
        /*
        Download friend request
         */
        if (!Utility.isConnectionAvailable(FriendRequestActivity.this)) {
            Utility.showAlertMessage(FriendRequestActivity.this);
            /*
            show the progress bar
             */
            mFriendReqProgressBar.setVisibility(View.VISIBLE);

        } else {
            downloadRequest(username_f_req);
        }

        /*
        Register the broadcast receiver
         */
        registerReceiver(mFriendReqBroadcastReceiver, mFriendReqIntentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
         /*
            this method unregister the message
         */
        unregisterReceiver(mFriendReqBroadcastReceiver);
    }

    /*
        Download request of friends
         */
    public void downloadRequest(final String username_f_req) {

        Request request = new Request.Builder()
                .url(Config.URL_SHOW_FRIEND_REQUEST + username_f_req + Config.token)
                .build();

        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String mResponse = response.body().string();

                    try {
                        JSONArray jsonArray = new JSONArray(mResponse);

                        List<FriendRequest> myRequestList = new ArrayList<>(jsonArray.length());

                        for (int i=0; i<jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String user_two = jsonObject.getString("user_two");
                            String user_one = jsonObject.getString("user_one");

                            mFriendRequest = new FriendRequest(user_two, user_one);
                            myRequestList.add(mFriendRequest);
                        }


                        /*
                            Adding  in the mFriendRequestList
                         */
                        mFriendRequestList = myRequestList;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mFriendRequestAdapter = new FriendRequestAdapter(mFriendRequestList, FriendRequestActivity.this, username_f_req, thisActivity);
                                mListViewFriendRequest.setAdapter(mFriendRequestAdapter);
                                mFriendRequestAdapter.notifyDataSetChanged();

                                mFriendReqProgressBar.setVisibility(View.GONE);
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

    }

}


