package rotate.com.aakarshrestha.rotate.Helper;

public class Comment {

    public String mCommenterName;
    public String mComment;
    public String mTimeStamp;

    public Comment() {
    }

    public Comment(String commenterName, String comment, String timeStamp) {
        mCommenterName = commenterName;
        mComment = comment;
        mTimeStamp = timeStamp;
    }

    public String getCommenterName() {
        return mCommenterName;
    }

    public void setCommenterName(String commenterName) {
        mCommenterName = commenterName;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getTimeStamp() {
        return mTimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        mTimeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return mCommenterName + " " + mComment + " " + mTimeStamp;
    }
}

