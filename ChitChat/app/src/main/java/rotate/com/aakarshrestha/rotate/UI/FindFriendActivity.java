package rotate.com.aakarshrestha.rotate.UI;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import rotate.com.aakarshrestha.rotate.Helper.Config;
import rotate.com.aakarshrestha.rotate.Helper.FriendsList;
import rotate.com.aakarshrestha.rotate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FindFriendActivity extends AppCompatActivity {

    protected ImageView mBackArrowImage;
    protected EditText mSearchBox;
    private ListView mListView;
    public ImageView mSearchIcon;

    private SharedPreferences sharedPreferences;

    private SharedPreferences friendReqGCMPreferences;
    private SharedPreferences.Editor mfriendEditor;

    public String okValue;
    private ArrayAdapter mAdapter;
    public OkHttpClient okHttpClient = new OkHttpClient();

    public static FindFriendActivity mFindFriendActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friend);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("");
        }
        setSupportActionBar(toolbar);

        /*
        Get intent value from GetCommentsAdapter.java file
         */

        Intent intent = getIntent();
        String friendNameFromComment = intent.getExtras().getString("friend_name_from_comment");

        mFindFriendActivity = this;

        mBackArrowImage = (ImageView) findViewById(R.id.backArrowIDimage);
        mListView = (ListView) findViewById(R.id.listView);
        mSearchBox = (EditText) findViewById(R.id.searchTextID);
        mSearchIcon = (ImageView) findViewById(R.id.searchIconID);

        /*
           If the value of the intent coming from GetCommentsAdapter is not null, then set the text in the search box automatically
         */
        if (friendNameFromComment != null) {
            mSearchBox.setText("");
            try {
                Thread.sleep(100);

                mSearchBox.setText(friendNameFromComment);
                if (mSearchBox.length() > 0) {
                    searchFriend();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //Animation on search box
        YoYo.with(Techniques.SlideInRight).duration(2100).playOn(mSearchBox);
        mSearchBox.setVisibility(View.VISIBLE);

        sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        final String mCurrentUserName = sharedPreferences.getString(Config.USERNAMEKEY, "User not found in find friend!");

        mBackArrowImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FindFriendActivity.this, ProfileActivity.class);
                startActivity(intent);

                /*
                Slide the activity
                */
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }

        });

        /*
        Search for user when the text is entered in the searchbox
         */
        searchFriend();


//Click on the list items to friend or un-friend
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (mListView.isItemChecked(position)) {

                    final String otherFriendNameToAdd = mListView.getItemAtPosition(position).toString();

                    friendReqGCMPreferences = getSharedPreferences("friendReqGCMPreferences", Context.MODE_PRIVATE);
                    mfriendEditor = friendReqGCMPreferences.edit();

                    Request request = new Request.Builder()
                            .url(Config.URL_USER_PROFILE + otherFriendNameToAdd + Config.token)
                            .build();

                    Call call = okHttpClient.newCall(request);
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {

                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {

                            if (response.isSuccessful()) {

                                String mResponseGCM = response.body().string();

                                try {
                                    JSONArray jsonArray = new JSONArray(mResponseGCM);
                                    for (int i=0; i<jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        final String friendGCMToken = jsonObject.getString("gcm_regid");

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mfriendEditor.putString("freqgcmpreferences", friendGCMToken);
                                                mfriendEditor.apply();

                                                addFriend(mCurrentUserName, otherFriendNameToAdd, friendGCMToken);

                                            }
                                        });
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        }
                    });

                } else {

                    final String otherFriendNameToRemove = mListView.getItemAtPosition(position).toString();

                     /*
                       Show alert message before removing the friend
                     */

                    AlertDialog.Builder builder = new AlertDialog.Builder(FindFriendActivity.this, R.style.AlertDialogTheme)
                            .setTitle("Removing friend")
                            .setMessage("Are you sure you want to remove your friend?")
                            .setNegativeButton("Cancel", null)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    FriendsList friendsList = new FriendsList();
                                    friendsList.removeFriend(mCurrentUserName, otherFriendNameToRemove);
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
            }
        });

    }

    public FindFriendActivity getFindFriendActivity() {
        return mFindFriendActivity;
    }

    //Add friend
    protected void addFriend(String userName, final String otherFriendUserName, String friendGCMToken) {

        RequestBody formBody = new FormBody.Builder()
                .add("username", userName)
                .add("otheruser_username", otherFriendUserName)
                .add("gcm_token", friendGCMToken)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_MAKE_FRIENDS)
                .post(formBody)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                try {
                    throw new IOException("Unexpected code");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Oops! Something went wrong!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Toast.makeText(getApplicationContext(), "Added to the friend list successfully!", Toast.LENGTH_SHORT).show();

                        String responsezAdd = null;
                        try {
                            responsezAdd = response.body().string();

                            JSONObject jsonObject = new JSONObject(responsezAdd);
                            if (jsonObject.names().get(0).equals("success")) {


                                Toast.makeText(getApplicationContext(), "Success: " + jsonObject.getString("success"), Toast.LENGTH_SHORT).show();

                            } else {

                                /*
                                    Show alert when the friendship is already established!
                                */

                                Toast.makeText(getApplicationContext(), "You are already friend with " + otherFriendUserName + ".", Toast.LENGTH_LONG).show();
//                              String title_friend_already = "Already friend...";
//                              Utility.friendErrorAlert(FindFriendActivity.this, jsonObject, title_friend_already);

                            }

                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

            }
        });

    }


    // Get other users list
    private ArrayList<String> getFriendList(String uStringName) {

        final ArrayList<String> aList = new ArrayList<>();

        Request request = new Request.Builder()
                .url(Config.URL + uStringName + Config.token)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {

                    okValue = response.body().string();

                    try {

                        JSONArray jsonArray = new JSONArray(okValue);


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String name = jsonObject.getString("username");
                            //Log.v("NAME", name);

                            aList.add(name);

                        }

                        for (String mylist : aList) {

                            /*
                                Don't show the user if the user is search him/herself.
                             */
                            String mCurrentUserName = sharedPreferences.getString(Config.USERNAMEKEY, "User not found in find friend!");

                            if (mylist.equals(mCurrentUserName)) {
                                aList.remove(mylist);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

        return aList;

    }

    private void searchFriend() {
        mSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable textWatcher) {

                //getValue(String.valueOf(s));

                if (textWatcher != null && textWatcher.length() >= 3) {
                    //getValue(String.valueOf(s));

                    mAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_multiple_choice, getFriendList(String.valueOf(textWatcher).trim())) {

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            // Get the Item from ListView
                            View view = super.getView(position, convertView, parent);

                            // Initialize a TextView for ListView each Item
                            TextView tv = (TextView) view.findViewById(android.R.id.text1);

                            // Set the text color of TextView (ListView Item)
                            tv.setTextColor(Color.BLACK);

                            // Generate ListView Item using TextView
                            return view;
                        }

                    };

                    mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                    mListView.setAdapter(mAdapter);

                }

            }
        });
    }

}

