package rotate.com.aakarshrestha.rotate.Helper;

public class UserProfile {

    private String mUsername;
    private String mEmail;
    private String mMessage;
    private String mDate;

    public UserProfile() {
    }

    public UserProfile(String username, String email) {
        mUsername = username;
        mEmail = email;
    }

    public UserProfile(String username, String email, String date) {
        mUsername = username;
        mEmail = email;
        mDate = date;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    @Override
    public String toString() {
        return mUsername;
    }
}

