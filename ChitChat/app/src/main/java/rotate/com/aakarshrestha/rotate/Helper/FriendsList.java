package rotate.com.aakarshrestha.rotate.Helper;

import android.widget.Toast;

import rotate.com.aakarshrestha.rotate.UI.FindFriendActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FriendsList {

    OkHttpClient okHttpClient = new OkHttpClient();

    //Accept friend request
    public void acceptFriendRequest(String userName, String otherFriendUserName) {

        RequestBody formBody = new FormBody.Builder()
                .add("username", userName)
                .add("otheruser_username", otherFriendUserName)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_ACCEPT_FRIEND_REQUEST)
                .post(formBody)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String responsezAdd = null;
                try {
                    responsezAdd = response.body().string();

                    JSONObject jsonObject = new JSONObject(responsezAdd);
                    if (jsonObject.names().get(0).equals("success")) {

                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    /*
    Reject friend request
     */
    public void rejectFriendRequest(String myUserName, final String friendUserName) {

        RequestBody formBody = new FormBody.Builder()
                .add("username", myUserName)
                .add("otheruser_username", friendUserName)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_DELETE_FRIENDS)
                .post(formBody)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                try {
                    throw new IOException("Unexpected code");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if (response.isSuccessful()) {

                    String responsezRemove = null;

                    try {

                        responsezRemove = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsezRemove);

                        if (jsonObject.names().get(0).equals("success")) {

                        }


                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

    }

    //Remove friend
    public void removeFriend(String myUserName, final String friendUserName) {

        RequestBody formBody = new FormBody.Builder()
                .add("username", myUserName)
                .add("otheruser_username", friendUserName)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_DELETE_FRIENDS)
                .post(formBody)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                try {
                    throw new IOException("Unexpected code");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if (response.isSuccessful()) {

                    String responsezRemove = null;

                    try {

                        responsezRemove = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsezRemove);

                        if (jsonObject.names().get(0).equals("success")) {
                                    /*
                                    remove the image of friend, who is removed from the friend list, from the folder
                                     */

                            if (FileManage.deleteFile(friendUserName, FindFriendActivity.mFindFriendActivity.getApplicationContext())) {

                                        /*
                                            Show alert when the friendship is deleted!
                                        */

//                                String title = "You un-friend successfully!";
//                                Utility.friendSuccessAlert(FindFriendActivity.mFindFriendActivity.getApplicationContext(), jsonObject, title);

                                FindFriendActivity.mFindFriendActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(FindFriendActivity.mFindFriendActivity.getApplicationContext(), "Friendship with [" + friendUserName + "] removed successfully!", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                        }


                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

    }

}


