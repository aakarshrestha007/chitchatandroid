package rotate.com.aakarshrestha.rotate.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import rotate.com.aakarshrestha.rotate.R;
import rotate.com.aakarshrestha.rotate.Helper.UserProfile;

import java.util.ArrayList;
import java.util.List;

public class FriendAdapter extends BaseAdapter {

    List<UserProfile> mUserProfileList = new ArrayList<>();
    Context mContext;
    LayoutInflater mInflater;
    ViewHolder mViewHolder;

    public FriendAdapter(List<UserProfile> userProfileList, Context context) {
        mUserProfileList = userProfileList;
        mContext = context;
        mInflater = (LayoutInflater.from(mContext));
    }

    @Override
    public int getCount() {
        return mUserProfileList.size();
    }

    @Override
    public Object getItem(int position) {
        return mUserProfileList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.friendview_item, null);
            mViewHolder = new ViewHolder();
            mViewHolder.mCheckBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.mCheckBox.setText(mUserProfileList.get(position).getUsername());

        return convertView;
    }

    static class ViewHolder {
        CheckBox mCheckBox;

    }
}


